package th.co.ktaxa.edoc.batch.etaxcert

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import th.co.ktaxa.edoc.batch.etaxcert.properties.EtaxcertProperties

@SpringBootApplication
@EnableConfigurationProperties(EtaxcertProperties::class)
class BatchApplication

fun main(args: Array<String>) {
	runApplication<BatchApplication>(*args)
}


