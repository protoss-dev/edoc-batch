package th.co.ktaxa.edoc.batch.etaxcert.service

import com.itextpdf.io.font.PdfEncodings
import com.itextpdf.io.image.ImageDataFactory
import com.itextpdf.kernel.colors.ColorConstants
import com.itextpdf.kernel.events.Event
import com.itextpdf.kernel.events.IEventHandler
import com.itextpdf.kernel.events.PdfDocumentEvent
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.geom.Rectangle
import com.itextpdf.kernel.pdf.*
import com.itextpdf.kernel.pdf.canvas.PdfCanvas
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject
import com.itextpdf.layout.Canvas
import com.itextpdf.layout.Document
import com.itextpdf.layout.borders.Border
import com.itextpdf.layout.borders.SolidBorder
import com.itextpdf.layout.element.Cell
import com.itextpdf.layout.element.Image
import com.itextpdf.layout.element.Paragraph
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.property.TextAlignment
import com.itextpdf.layout.property.UnitValue
import com.itextpdf.svg.converter.SvgConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.PaperSize
import th.co.ktaxa.edoc.batch.etaxcert.properties.EtaxcertProperties
import java.io.*

@Service
@Component
class Template6PdfServiceImpl(@Autowired private val properties: EtaxcertProperties) : TemplatePdfService(properties) {

    @Throws(IOException::class)
    override  fun pdfInputStream(taxcerts: List<TemplateModel>, password: String?): ByteArrayInputStream {
        val thSarabunNewRegular = "templates/font/THSarabunNew.ttf"
        val thSarabunNewBold = "templates/font/THSarabunNew-Bold.ttf"
        val signaturePath = "templates/signed/signature.png"
        val out = ByteArrayOutputStream()
        var writer: PdfWriter

        if (password != null && password.trim() != "") {
            val props = WriterProperties().setStandardEncryption(password.toByteArray(), password.toByteArray(),
                    EncryptionConstants.ALLOW_PRINTING,
                    EncryptionConstants.ENCRYPTION_AES_128 or EncryptionConstants.DO_NOT_ENCRYPT_METADATA)
            writer = PdfWriter(out, props)
        } else {
            writer = PdfWriter(out)
        }

        val pdf = PdfDocument(writer)

        val fontRegular = PdfFontFactory.createFont(ClassPathResource(thSarabunNewRegular).path, PdfEncodings.IDENTITY_H)
        val fontBold = PdfFontFactory.createFont(ClassPathResource(thSarabunNewBold).path, PdfEncodings.IDENTITY_H)
        val imgSignature = Image(ImageDataFactory.create(ClassPathResource(signaturePath).inputStream.readAllBytes()))

        // Creating a Document
        val document = Document(pdf, PaperSize.TEMPLATE_6.pageSize)
        document.topMargin = PaperSize.TEMPLATE_6.topMargin +61f
        document.leftMargin = PaperSize.TEMPLATE_6.leftMargin
        document.rightMargin = PaperSize.TEMPLATE_6.rightMargin
        document.bottomMargin = PaperSize.TEMPLATE_6.bottomMargin

        val columnWidthsHeader = floatArrayOf(100f)
        val tableLayout = Table(UnitValue.createPercentArray(columnWidthsHeader))
        tableLayout.setWidth(UnitValue.createPercentValue(100f));

        val pageXofYEventHandler = PageXofY()
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, pageXofYEventHandler)

        /*
         * Section POLICY
         */
        val columnWidthsPolicy = floatArrayOf(7f, 53f, 20f, 20f)
        val tablePolicy = Table(UnitValue.createPercentArray(columnWidthsPolicy))
        tablePolicy.setWidth(UnitValue.createPercentValue(100f));

        // Name [WNAME] OR [WISSNAM]
        var dear : String? = taxcerts[0].WNAME
        var hasOwner : Boolean = true
        if( taxcerts[0].WNAME.isNullOrBlank() ){
            dear = taxcerts[0].WISSNAM
            hasOwner = false
        }

        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เรียน").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(dear).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))
        // Policy [WPNO]
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("กรมธรรม์เลขที่:").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WPNO).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))

        // Address1
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER))
        //[WADR1]
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WADR1).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))
        // Effective Date [WCEFF]
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("วันที่เริ่มทำสัญญา:").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WCEFF).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))

        // Address2
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER))
        //[WADR2]
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WADR2).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))
        // Note Period
        tablePolicy.addCell(Cell(1, 2).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("กำหนดระยะเวลาเอาประกันภัยเป็นแบบ 10 ปี ขึ้นไป").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))

        // Address3
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER))
        //[WADR3]
        tablePolicy.addCell(Cell(1, 3).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WADR3).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))

        // Address4
        tablePolicy.addCell(Cell(1, 1).setBorder(Border.NO_BORDER))
        //[WADR4]
        tablePolicy.addCell(Cell(1, 3).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WADR4).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))

        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(tablePolicy))

        /*
         * Section HEADER TITLE
         */
        // Title
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.CENTER)
                .add(Paragraph("หนังสือรับรองการชำระเบี้ยประกันภัย").setFixedLeading(18f).setFont(fontBold).setFontSize(16f)))
        // Line 7
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.CENTER).setPaddingTop(5f)
                .add(Paragraph("เพื่อเป็นหลักฐานหรือใช้ประกอบการหักลดหย่อนภาษีเงินได้ตามเงื่อนไข").setFixedLeading(10f).setFont(fontRegular).setFontSize(12f)))
        // Line 8
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.CENTER)
                .add(Paragraph("(เฉพาะผู้เอาประกันภัยที่มีเงินได้พึงประเมินและจ่ายชำระค่าเบี้ยประกันจริงเพื่อเสียภาษีตามเกณฑ์ที่สรรพากรกำหนดเท่านั้น)").setFixedLeading(12f).setFont(fontRegular).setFontSize(12f)))

        /*
         * Section CUSTOMER
         */
        val columnWidthsCustomer = floatArrayOf(10f, 60f, 15f, 15f)
        val tableCustomer = Table(UnitValue.createPercentArray(columnWidthsCustomer))
        tableCustomer.setWidth(UnitValue.createPercentValue(100f));

        // Assured [WISSNAM]
        tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("ผู้เอาประกันภัย").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
        tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WISSNAM).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))
        // Assured National ID [WPID]
        tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("หมายเลขบัตรประชาชน").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
        tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(taxcerts[0].WPID).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))


        var countOfLine : Int = 14
        if(hasOwner){
            countOfLine--
            // Owner [WNAME]
            tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(Paragraph("ผู้ชำระเบี้ย").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
            tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(Paragraph(taxcerts[0].WNAME).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))

            // Owner National ID [WPOWNID]
            tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(Paragraph("หมายเลขบัตรประชาชน").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
            tableCustomer.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(Paragraph(taxcerts[0].WPOWNID).setFixedLeading(12f).setFont(fontBold).setFontSize(14f)))
        }

        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT).setPaddingTop(10f)
                .add(tableCustomer))

        /*
         * Section EVIDENT
         */
        // Evident Title
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("ตามหลักฐานของบริษัท ได้ชำระเบี้ยประกันภัยในระหว่างปี พ.ศ. 2562").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setMarginLeft(100f)))
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("บริษัท ได้รับการชำระเบี้ยประกันภัย มีรายละเอียดดังนี้ :-").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setMarginLeft(100f)))

        /*
         * Section TRANSACTION
         */
        val columnWidthsTransaction = floatArrayOf(10f, 35f, 15f, 15f, 15f)
        val tableTransaction = Table(UnitValue.createPercentArray(columnWidthsTransaction))
        tableTransaction.setWidth(UnitValue.createPercentValue(90f));

        val headerTransaction = arrayOf<Cell>(
                Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f))
                        .add(Paragraph("ปีที่").setFixedLeading(14f).setFont(fontBold).setFontSize(14f).setPaddingTop(5f).setPaddingBottom(5f)),
                Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f))
                        .add(Paragraph("วันถึงกำหนดชำระเบี้ยประกันภัย\n(วัน/เดือน/ปี)").setFixedLeading(14f).setFont(fontBold).setFontSize(14f).setPaddingTop(5f).setPaddingBottom(5f)),
                Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f))
                        .add(Paragraph("วันที่ชำระ\n(วัน/เดือน/ปี)").setFixedLeading(14f).setFont(fontBold).setFontSize(14f).setPaddingTop(5f).setPaddingBottom(5f)),
                Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f))
                        .add(Paragraph("ใบเสร็จรับเงิน\nเลขที่").setFixedLeading(14f).setFont(fontBold).setFontSize(14f).setPaddingTop(5f).setPaddingBottom(5f)),
                Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f))
                        .add(Paragraph("จำนวนเงิน").setFixedLeading(14f).setFont(fontBold).setFontSize(14f).setPaddingTop(5f).setPaddingBottom(5f))
        )
        for (header in headerTransaction) {
            tableTransaction.addHeaderCell(header)
        }

        var currentPage = 1;
        var indexRecord = 0;
        var countRecord = 0;
        var recordOfCurrentPage = 0;
        for (taxcert in taxcerts) {
                indexRecord++;
                countRecord++;
                tableTransaction.addCell(Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f)).add(Paragraph(taxcert.WTERM.toString()).setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
                tableTransaction.addCell(Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f)).add(Paragraph(taxcert.WDUEDT).setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
                tableTransaction.addCell(Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f)).add(Paragraph(taxcert.WPAYDT).setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
                tableTransaction.addCell(Cell().setTextAlignment(TextAlignment.CENTER).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f)).add(Paragraph(taxcert.WRNO).setFixedLeading(12f).setFont(fontRegular).setFontSize(14f)))
                tableTransaction.addCell(Cell().setTextAlignment(TextAlignment.RIGHT).setBorder(SolidBorder(ColorConstants.BLACK, 0.5f)).add(Paragraph(parenthesesIfNegative((taxcert.WAMT!!))).setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingRight(10f)))
        
                if(currentPage == 1 ){

                        
                        if(taxcerts.size > countOfLine && taxcerts.size < (countOfLine+11) && indexRecord == (taxcerts.size - 1)){
                                
                                var padding = when (taxcerts.size) {
                                        (countOfLine+1) -> 155f
                                        (countOfLine+2) -> 140f
                                        (countOfLine+3) -> 130f
                                        (countOfLine+4) -> 110f
                                        (countOfLine+5) -> 90f
                                        (countOfLine+6) -> 75f
                                        (countOfLine+7) -> 55f
                                        (countOfLine+8) -> 40f
                                        (countOfLine+9) -> 25f
                                        (countOfLine+10) -> 10f
                                        else -> 0f
                                }


                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                currentPage++;
                                indexRecord = 0
                        }else if(taxcerts.size > (countOfLine+11) && indexRecord == (countOfLine+11)){
                                currentPage++;
                                indexRecord = 0
                        }
                }else if(currentPage > 1){
                        if(recordOfCurrentPage == 0) {
                                recordOfCurrentPage = taxcerts.size - ( countRecord - 2 )
                        }


                        
                        if(recordOfCurrentPage > 26 && recordOfCurrentPage < 37 && (indexRecord - 1 ) == (recordOfCurrentPage - 3)){
                                var padding = when ((recordOfCurrentPage - 2)) {
                                                    25 -> 155f
                                                    26 -> 140f
                                                    27 -> 130f
                                                    28 -> 110f
                                                    29 -> 90f
                                                    30 -> 75f
                                                    31 -> 55f
                                                    32 -> 40f
                                                    33 -> 25f
                                                    34 -> 10f
                                                    else -> 0f
                                                }

                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                tableTransaction.addCell(Cell().setBorder(Border.NO_BORDER).add(Paragraph(" ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f).setPaddingBottom(padding)) )
                                currentPage++;
                                indexRecord = 0
                        }else if(indexRecord == 36 ){
                                recordOfCurrentPage = 0
                                currentPage++;
                                indexRecord = 0
                        }
                }
        }

        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setPaddingLeft(30f).add(tableTransaction))

        /*
         * Section TOTAL
         */
        // Total Title
        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("รวมเป็นจำนวนเงินชำระ " + parenthesesIfNegative((taxcerts[0].TOTAMT!!)) + " บาท").setPaddingRight(10f).setFont(fontRegular).setFontSize(14f)))

        /*
         * Section FOOTER
         */
        val columnWidthsFooter = floatArrayOf(15f, 5f, 15f, 50f, 15f)
        val tableFooter = Table(UnitValue.createPercentArray(columnWidthsFooter))

        val signatureCell = Cell(6, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.CENTER)
                .add(Paragraph("ขอแสดงความนับถือ").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f))
                .add(imgSignature.setWidth(75f).setMarginLeft(8f))
                .add(Paragraph("(กิติมา เอ็นดู)").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f))
                .add(Paragraph("ผู้มีอำนาจลงนาม").setFixedLeading(12f).setFont(fontRegular).setFontSize(14f))

        // Footer Line 1
        tableFooter.addCell(Cell(1, 4).setBorder(Border.NO_BORDER))
        tableFooter.addCell(signatureCell)

        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เบี้ยประกันชีวิต *").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("จำนวนเงิน").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph(parenthesesIfNegative((taxcerts[0].TAX!!)) + " บาท").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("สามารถนำไปหักลดหย่อนภาษีเงินได้บุคคลธรรมดา").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        // Footer Line 2
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เบี้ยประกันภัยสุขภาพ **").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("จำนวนเงิน").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph(parenthesesIfNegative((taxcerts[0].HTAX!!)) + " บาท").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("สามารถนำไปหักลดหย่อนภาษีเงินได้บุคคลธรรมดา").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        // Footer Line 3
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เบี้ยประกันภัยในส่วนความคุ้มครองอื่นๆ").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("จำนวนเงิน").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph(parenthesesIfNegative((taxcerts[0].NOTAXOTH!!)) + " บาท").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("ไม่สามารถนำไปหักลดหย่อนภาษีเงินได้บุคคลธรรมดา").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        // Footer Line 4
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เบี้ยประกันภัยตามสัญญาเพิ่มเติมอื่นๆ").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("จำนวนเงิน").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph(parenthesesIfNegative((taxcerts[0].NOTAX!!)) + " บาท").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("ไม่สามารถนำไปหักลดหย่อนภาษีเงินได้บุคคลธรรมดา").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        // Footer Line 5
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("เบี้ยประกันภัยรวม").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph("จำนวนเงิน").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.RIGHT)
                .add(Paragraph(parenthesesIfNegative((taxcerts[0].TOTAMT!!)) + " บาท").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))
        tableFooter.addCell(Cell(1, 1).setBorder(Border.NO_BORDER))



        // Footer Line 6
        tableFooter.addCell(Cell(1, 5).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("หมายเหตุ : *เบี้ยประกันชีวิตสำหรับอ้างอิงการใช้สิทธิลดหย่อนภาษีเงินได้บุคคลธรรมดาตามกฎหมาย (กรมธรรม์ประกันชีวิตฉบับ\n" +
                        "นี้ทำหลังปี พ.ศ. 2551) แบบสิบปีขึ้นไป และเงินคืนตามเงื่อนไขของกรมธรรม์ประกันชีวิต (ถ้ามี) เป็นไปตามประกาศอธิบดี\n" +
                        "กรมสรรพากร ฉบับที่ 172 ข้อ 2(2)").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        // Footer Line 7
        tableFooter.addCell(Cell(1, 5).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph(" **เบี้ยประกันสุขภาพภายใต้เบี้ยประกันภัยตามสัญญาเพิ่มเติมสุขภาพสำหรับอ้างอิงการใช้สิทธิลดหย่อนภาษีเงินได้บุคคลธรรมดา").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f).setMarginLeft(30f)))

        // Footer Line 8
        tableFooter.addCell(Cell(1, 5).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                .add(Paragraph("ตามกฎหมายที่จ่ายตั้งแต่ปี พ.ศ.2560 โดยเป็นไปตามเงื่อนไขข้อกำหนดของกรมสรรพากร ฉบับที่ 315").setFixedLeading(10f).setFont(fontRegular).setFontSize(10f)))

        tableLayout.addCell(Cell(1, 1).setBorder(Border.NO_BORDER).add(tableFooter))

        document.add(tableLayout)

        // Closing the document
        document.close()

        return ByteArrayInputStream(out.toByteArray())
    }

    class PageXofY : IEventHandler {
        private val side = 40f
        private val placeholder: PdfFormXObject = PdfFormXObject(Rectangle(0f, 0f, side, side))
        private val thSarabunNewRegular = "templates/font/THSarabunNew.ttf"
        private val thSarabunNewBold = "templates/font/THSarabunNew-Bold.ttf"

        private val x = 580f
        private val y = 10f
        private val space = 0f
        private val descent = 0f

        override fun handleEvent(event: Event) {
            val docEvent = event as PdfDocumentEvent
            val pdf = docEvent.document
            val logoPath = "templates/signed/logo_th.svg"

            val svg = ClassPathResource(logoPath).inputStream
            val fontRegular = PdfFontFactory.createFont(ClassPathResource(thSarabunNewRegular).path, PdfEncodings.IDENTITY_H)
            val fontBold = PdfFontFactory.createFont(ClassPathResource(thSarabunNewBold).path, PdfEncodings.IDENTITY_H)
            val imgLogo = SvgConverter.convertToImage(svg, pdf)

            val page = docEvent.page
            val pageNumber = pdf.getPageNumber(page)
            val totalPages = docEvent.document.numberOfPages
            val pageSize = page.pageSize
            val pdfCanvas = PdfCanvas(page.lastContentStream, page.resources, pdf)
            val canvas = Canvas(pdfCanvas, pdf, pageSize)

            val document = Document(pdf, PaperSize.TEMPLATE_6.pageSize)
            document.topMargin = PaperSize.TEMPLATE_6.topMargin+2f
            document.leftMargin = PaperSize.TEMPLATE_6.leftMargin+2f
            document.rightMargin = PaperSize.TEMPLATE_6.rightMargin
            document.bottomMargin = PaperSize.TEMPLATE_6.bottomMargin

            /*
             * Section Page Header
             */
            val columnWidthsLogo = floatArrayOf(10f, 90f)
            val tableLogo = Table(UnitValue.createPercentArray(columnWidthsLogo))
            tableLogo.setWidth(UnitValue.createPercentValue(100f));

            tableLogo.addCell(Cell(1,1).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(imgLogo.setWidth(100f)))
            tableLogo.addCell(Cell(1,2).setBorder(Border.NO_BORDER).setTextAlignment(TextAlignment.LEFT)
                    .add(Paragraph("บริษัท กรุงไทย-แอกซ่า ประกันชีวิต จำกัด (มหาขน)").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontBold).setFontSize(12f))
                    .add(Paragraph("เลขที่ 9 อาคาร จี ทาวเวอร์ แกรนด์ รามา 9 ชั้น 1, 20-27\n" +
                            "ถนนพระราม 9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310\n" +
                            "โทร. 0 2044 4000 ศูนย์ลูกค้าสัมพันธ์ โทร. 1159 www.krungthai-axa.co.th\n" +
                            "ทะเบียนบริษัท/เลขประจำตัวผู้เสียภาษีบริษัท 0107555000376").setPaddingLeft(5f).setFixedLeading(10f).setFont(fontRegular).setFontSize(10f))
            )
            document.add(tableLogo)

            /*
             * Section Page Footer
             */
            val company = Paragraph()
                    .add(Paragraph()
                            .add("บริษัท กรุงไทย-แอกซ่า ประกันชีวิต จำกัด (มหาขน) - Krungthai-AXA Life Insurance Public Company Limited\n").setFont(fontBold).setFontSize(12F))
            val address = Paragraph()
                    .add(Paragraph()
                            .add("เลขที่ 9 อาคาร จี ทาวเวอร์ แกรนด์ รามา 9 ชั้น 1, 20-27 ถนนพระราม 9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310\n" +
                                    "9, G Tower Grand Rama 9, Floor 1,20-27 Rama 9 Road, Huai Khwang, Huai Khwang, Bangkok 10310\n" +
                                    "โทร./Tel. 0 2044 4000 โทรสาร 0 2044 4032 ศูนย์ลูกค้าสัมพันธ์/Customer Center โทร./Tel. 1159 เลขทะเบียนบริษัทฯ 0107555000376 www.krungthai-axa.co.th").setFont(fontRegular).setFontSize(10F).setFixedLeading(10f))

            canvas.showTextAligned(company, PaperSize.TEMPLATE_6.leftMargin+4f, y+45f, TextAlignment.LEFT)
            canvas.showTextAligned(address, PaperSize.TEMPLATE_6.leftMargin+4f, y+15f, TextAlignment.LEFT)

            val p = Paragraph().setFont(fontRegular).setFontSize(14F).add("หน้าที่ ${pageNumber}/${totalPages}")
            canvas.showTextAligned(p, x, y, TextAlignment.RIGHT)
            pdfCanvas.addXObject(placeholder, x + space, y - descent)
            pdfCanvas.release()
        }
    }
}