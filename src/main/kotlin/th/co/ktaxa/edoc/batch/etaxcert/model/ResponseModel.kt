package th.co.ktaxa.edoc.batch.etaxcert.model

import com.fasterxml.jackson.annotation.JsonInclude

data class ResponseModel (
        val success: Boolean,
        val message: String,
        @JsonInclude(JsonInclude.Include.NON_NULL)
        var errorCode: String? = null,
        val data: Any
)