package th.co.ktaxa.edoc.batch.etaxcert.worker

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.DigitialSigned
import th.co.ktaxa.edoc.batch.etaxcert.properties.EtaxcertProperties
import th.co.ktaxa.edoc.batch.etaxcert.service.PdfService

class TemplateWorkerThread(private val taxcerts: List<TemplateModel>,
                           private val pdfService: PdfService,
                           private val properties: EtaxcertProperties) : Runnable {
    var logger: Logger = LoggerFactory.getLogger(TemplateWorkerThread::class.java)

    override fun run() {
        logger.info("Generating policy no.: ${taxcerts[0].WPNO} in thread ${Thread.currentThread().name}")
        processMessage()
    }

    private fun processMessage() {
        try {
            val file: String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
            val pdf = pdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
            logger.info("... Location: ${pdf}")
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

}