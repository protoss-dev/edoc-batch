package th.co.ktaxa.edoc.batch.etaxcert.model.enum

import com.itextpdf.kernel.geom.PageSize

enum class PaperSize(val pageSize: PageSize, val topMargin: Float, val leftMargin: Float, val rightMargin: Float, val bottomMargin: Float) {
    TEMPLATE_1(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_2(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_3(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_4(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_5(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_6(PageSize.A4, 54f, 54f, 36f, 70f),
    TEMPLATE_7(PageSize.A4, 54f, 54f, 36f, 70f)
}