package th.co.ktaxa.edoc.batch.etaxcert.utility

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@SpringBootTest
class DateFormatterUtilityTest {

    @Test
    fun formatDateTimeTest() {
        val date = LocalDateTime.parse("2020-05-08 11:30:23", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        assertThat(date.format(DateFormatterUtility.formatter1)).isEqualTo("08052020")
        assertThat(date.format(DateFormatterUtility.formatter2)).isEqualTo("2020-05-08")
        assertThat(date.format(DateFormatterUtility.formatter3)).isEqualTo("08/05/20")
        assertThat(date.format(DateFormatterUtility.formatter4)).isEqualTo("08/05/2020")
        assertThat(date.format(DateFormatterUtility.formatter5)).isEqualTo("20200508")
        assertThat(date.format(DateFormatterUtility.formatter6)).isEqualTo("08 May 20")
        assertThat(date.format(DateFormatterUtility.formatter7)).isEqualTo("11:30:23 AM")
        assertThat(date.format(DateFormatterUtility.formatter8)).isEqualTo("08 May 2020")
        assertThat(date.format(DateFormatterUtility.formatter9)).isEqualTo("8 พฤษภาคม 2563")
    }

    @Test
    fun toThaiMonthTest() {
        assertThat(DateFormatterUtility.toDateWithThaiMonth("")).isEqualTo("")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 01 2020")).isEqualTo("8 มกราคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 02 2020")).isEqualTo("8 กุมภาพันธ์ 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 03 2020")).isEqualTo("8 มีนาคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 04 2020")).isEqualTo("8 เมษายน 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 05 2020")).isEqualTo("8 พฤษภาคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 06 2020")).isEqualTo("8 มิถุนายน 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 07 2020")).isEqualTo("8 กรกฎาคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 08 2020")).isEqualTo("8 สิงหาคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 09 2020")).isEqualTo("8 กันยายน 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 10 2020")).isEqualTo("8 ตุลาคม 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 11 2020")).isEqualTo("8 พฤศจิกายน 2020")
        assertThat(DateFormatterUtility.toDateWithThaiMonth("08 12 2020")).isEqualTo("8 ธันวาคม 2020")
    }

    @Test
    fun toDateWithThaiMonthAndBuddhistYearTest() {
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("")).isEqualTo("")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 01 2020")).isEqualTo("8 มกราคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 02 2020")).isEqualTo("8 กุมภาพันธ์ 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 03 2020")).isEqualTo("8 มีนาคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 04 2020")).isEqualTo("8 เมษายน 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 05 2020")).isEqualTo("8 พฤษภาคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 06 2020")).isEqualTo("8 มิถุนายน 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 07 2020")).isEqualTo("8 กรกฎาคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 08 2020")).isEqualTo("8 สิงหาคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 09 2020")).isEqualTo("8 กันยายน 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 10 2020")).isEqualTo("8 ตุลาคม 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 11 2020")).isEqualTo("8 พฤศจิกายน 2563")
        assertThat(DateFormatterUtility.toDateWithThaiMonthAndBuddhistYear("08 12 2020")).isEqualTo("8 ธันวาคม 2563")
    }

    @Test
    fun toDateWithBuddhistYearTest() {
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("")).isEqualTo("")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("8/01/2020")).isEqualTo("8/01/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("9/02/2020")).isEqualTo("9/02/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("10/03/2020")).isEqualTo("10/03/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("11/04/2020")).isEqualTo("11/04/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("12/05/2020")).isEqualTo("12/05/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("13/06/2020")).isEqualTo("13/06/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("20/07/2020")).isEqualTo("20/07/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("21/08/2020")).isEqualTo("21/08/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("22/09/2020")).isEqualTo("22/09/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("23/10/2020")).isEqualTo("23/10/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("24/11/2020")).isEqualTo("24/11/2563")
        assertThat(DateFormatterUtility.toDateWithBuddhistYear("1/12/2020")).isEqualTo("1/12/2563")
    }

    @Test
    fun getThaiDateTest() {
        assertThat(DateFormatterUtility.getThaiDate("20200529")).isEqualTo("29 พฤษภาคม 2563")
    }

}