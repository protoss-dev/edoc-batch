package th.co.ktaxa.edoc.batch.etaxcert.service

import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.DigitialSigned
import java.io.ByteArrayInputStream

interface PdfService {
    fun savePdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String
    fun generatePdf(taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned): ByteArrayInputStream
    fun storePdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String
    fun storeApiPdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String
}