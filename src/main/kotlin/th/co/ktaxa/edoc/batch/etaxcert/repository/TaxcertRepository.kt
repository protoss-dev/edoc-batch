package th.co.ktaxa.edoc.batch.etaxcert.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import th.co.ktaxa.edoc.batch.etaxcert.model.dto.PolicyDto
import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel

/*
@Repository
interface TaxcertRepository : PagingAndSortingRepository<TemplateModel, Long> {
    fun findByWPNO(WPNO: String): List<TemplateModel>
}
*/

@Repository
interface TaxcertRepository : JpaRepository<TemplateModel, String> , JpaSpecificationExecutor<TemplateModel> {
    @Query("SELECT new th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel" +
                "(ID, WPNO, WPID, WPOWNID, WNAME, WADR1, WADR2, WADR3, WADR4, WCEFF, WISSNAM, WSTRDAT, WENDDAT, " +
                "WTERM, WDUEDT, WPAYDT, WRNO, WAMT, WMODE, WACT, WPMTH, TRANNO, TAX, NOTAXOTH, NOTAX, HTAX, TOTAMT, TEMPLATE) " +
            "FROM TemplateModel " +
            "WHERE (WPNO = :wpno) " +
            "ORDER BY WRNO ASC")
    fun findByWPNO(@Param("wpno") wpno: String): List<TemplateModel>

    @Query("SELECT new th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel" +
                "(ID, WPNO, WPID, WPOWNID, WNAME, WADR1, WADR2, WADR3, WADR4, WCEFF, WISSNAM, WSTRDAT, WENDDAT, " +
                "WTERM, WDUEDT, WPAYDT, WRNO, WAMT, WMODE, WACT, WPMTH, TRANNO, TAX, NOTAXOTH, NOTAX, HTAX, TOTAMT, TEMPLATE) " +
            "FROM TemplateModel " +
            "ORDER BY TEMPLATE, WPNO, WRNO ASC")
    override fun findAll(): List<TemplateModel>

    @Query("SELECT DISTINCT new th.co.ktaxa.edoc.batch.etaxcert.model.dto.PolicyDto(WPNO, TEMPLATE) " +
            "FROM TemplateModel")
    fun findAllPolicy(): List<PolicyDto>


}