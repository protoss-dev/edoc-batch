package th.co.ktaxa.edoc.batch.etaxcert.utility

import java.time.LocalDate
import java.time.chrono.ThaiBuddhistChronology
import java.time.format.DateTimeFormatter
import java.util.*

class DateFormatterUtility {

    companion object {

        val formatter1: DateTimeFormatter = DateTimeFormatter.ofPattern("ddMMyyyy")
        val formatter2: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val formatter3: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yy")
        val formatter4: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val formatter5: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")
        val formatter6: DateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM yy")
        val formatter7: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss a")
        val formatter8: DateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy")
        val formatter9: DateTimeFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy", Locale("TH","TH")).withChronology(ThaiBuddhistChronology.INSTANCE)

        enum class Month(val monthInt: String, val monthEng: String, val monthThai: String) {

            JAN("01","January", "มกราคม"),
            FEB("02","February", "กุมภาพันธ์"),
            MAR("03","March", "มีนาคม"),
            APR("04", "April", "เมษายน"),
            MAY("05", "May", "พฤษภาคม"),
            JUN("06", "June", "มิถุนายน"),
            JUL("07", "July", "กรกฎาคม"),
            AUG("08", "August", "สิงหาคม"),
            SEP("09", "September", "กันยายน"),
            OCT("10", "October", "ตุลาคม"),
            NOV("11", "November", "พฤศจิกายน"),
            DEC("12", "December", "ธันวาคม"),

        }

        private fun toThaiMonth(monthInt: String): String{
            return when(monthInt){
                Month.JAN.monthInt -> Month.JAN.monthThai
                Month.FEB.monthInt -> Month.FEB.monthThai
                Month.MAR.monthInt -> Month.MAR.monthThai
                Month.APR.monthInt -> Month.APR.monthThai
                Month.MAY.monthInt -> Month.MAY.monthThai
                Month.JUN.monthInt -> Month.JUN.monthThai
                Month.JUL.monthInt -> Month.JUL.monthThai
                Month.AUG.monthInt -> Month.AUG.monthThai
                Month.SEP.monthInt -> Month.SEP.monthThai
                Month.OCT.monthInt -> Month.OCT.monthThai
                Month.NOV.monthInt -> Month.NOV.monthThai
                Month.DEC.monthInt -> Month.DEC.monthThai
                else -> ""
            }
        }

        //8 05 2019 -> 8 พฤษภาคม 2019
        fun toDateWithThaiMonth(dateSting: String): String {

            return when(dateSting.isNotEmpty()){
                true -> {
                    val year = dateSting.drop(dateSting.length - 4)
                    val month = dateSting.drop(dateSting.length - 7).dropLast(5)
                    val date = dateSting.dropLast(8).toInt()
                    val thaiMonth = toThaiMonth(month)

                    "$date $thaiMonth $year"
                }
                else -> ""
            }
        }

        //8 05 2019 -> 8 พฤษภาคม 2562
        fun toDateWithThaiMonthAndBuddhistYear(dateSting: String): String {

            return when(dateSting.isNotEmpty()){
                true -> {
                    val year = dateSting.drop(dateSting.length - 4).toInt() + 543
                    val month = dateSting.drop(dateSting.length - 7).dropLast(5)
                    val date = dateSting.dropLast(8).toInt()
                    val thaiMonth = toThaiMonth(month)

                    "$date $thaiMonth $year"
                }
                else -> ""
            }
        }

        //03/04/2019 -> 03/04/2562
        fun toDateWithBuddhistYear(dateSting: String): String {

            return when(dateSting.isNotEmpty()){
                true -> {
                    val year = dateSting.drop(dateSting.length - 4).toInt() + 543
                    val month = dateSting.drop(dateSting.length - 7).dropLast(5)
                    val date = dateSting.dropLast(8).toInt()

                    "$date/$month/$year"
                }
                else -> ""
            }
        }

        //20190101 -> 1 มกราคม 2562
        fun getThaiDate(inputDate: String) : String{
            val date  =  LocalDate.parse(inputDate, formatter5)
            return date.format(formatter9)
        }
    }




}