package th.co.ktaxa.edoc.batch.etaxcert.auth

data class RealmAccess(
        val roles: List<String>
)