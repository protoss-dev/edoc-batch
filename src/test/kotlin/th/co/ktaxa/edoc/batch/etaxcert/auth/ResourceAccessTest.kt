package th.co.ktaxa.edoc.batch.etaxcert.auth

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ResourceAccessTest {

    @Test
    fun initialData() {
        val resourceAccess = ResourceAccess(Account(listOf("user","hr")))

        assertThat(resourceAccess.account).isEqualTo(Account(listOf("user","hr")))
    }
}