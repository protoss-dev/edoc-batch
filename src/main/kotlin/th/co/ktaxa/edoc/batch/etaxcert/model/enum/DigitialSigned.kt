package th.co.ktaxa.edoc.batch.etaxcert.model.enum

enum class DigitialSigned {
    NO_SIGNED,
    SIGNED,
    SIGNED_WITH_APPEARANCE
}