package th.co.ktaxa.edoc.batch.etaxcert.auth

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class RealmAccessTest {

    @Test
    fun initialData() {
        val r = RealmAccess(listOf("user","password"))

        assertThat(r.roles).isEqualTo(listOf("user","password"))
    }
}