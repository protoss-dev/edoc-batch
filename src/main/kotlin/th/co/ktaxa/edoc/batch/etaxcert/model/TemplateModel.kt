package th.co.ktaxa.edoc.batch.etaxcert.model

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "TC_TAX_CERT_DATA")
data class TemplateModel(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="ID", nullable = false)
        var ID: Long?,

        @Column(name="WPNO")
        var WPNO: String? = null,

        @Column(name="WPID")
        var WPID: String? = null,

        @Column(name="WPOWNID")
        var WPOWNID: String? = null,

        @Column(name="WNAME")
        var WNAME: String? = null,

        @Column(name="WADDR1")
        var WADR1: String? = null,

        @Column(name="WADDR2")
        var WADR2: String? = null,

        @Column(name="WADDR3")
        var WADR3: String? = null,

        @Column(name="WADDR4")
        var WADR4: String? = null,

        @Column(name="WCEFF")
        var WCEFF: String? = null,

        @Column(name="WISSNAM")
        var WISSNAM: String? = null,

        @Column(name="WSTRDAT")
        var WSTRDAT: String? = null,

        @Column(name="WENDDAT")
        var WENDDAT: String? = null,

        @Column(name="WTERM")
        var WTERM: Int? = 0,

        @Column(name="WDUEDT")
        var WDUEDT: String? = null,

        @Column(name="WPAYDT")
        var WPAYDT: String? = null,

        @Column(name="WRNO")
        var WRNO: String? = null,

        @Column(name="WAMT")
        var WAMT: BigDecimal? = BigDecimal.ZERO,

        @Column(name="WMODE")
        var WMODE: String? = null,

        @Column(name="WACT", nullable = false)
        var WACT: String? = null,

        @Column(name="WPMTH")
        var WPMTH: Int? = 0,

        @Column(name="TRANNO")
        var TRANNO: Int? = 0,

        @Column(name="TAX")
        var TAX: BigDecimal? = BigDecimal.ZERO,

        @Column(name="NOTAXOTH")
        var NOTAXOTH: BigDecimal? = BigDecimal.ZERO,

        @Column(name="NOTAX")
        var NOTAX: BigDecimal? = BigDecimal.ZERO,

        @Column(name="HTAX")
        var HTAX: BigDecimal? = BigDecimal.ZERO,

        @Column(name="TOTAMT")
        var TOTAMT: BigDecimal? = BigDecimal.ZERO,

        @Column(name="TEMPLATE", nullable = false)
        var TEMPLATE: Int? = 0
)