package th.co.ktaxa.edoc.batch.etaxcert.auth

data class ResourceAccess(
        val account: Account
)