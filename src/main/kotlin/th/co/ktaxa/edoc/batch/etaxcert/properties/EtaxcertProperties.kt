package th.co.ktaxa.edoc.batch.etaxcert.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("etaxcert")
data class EtaxcertProperties(val location: String, val pdf: PDF, val digitalSigned: DigitialSigned? = null,val bucketName: String,val pathUrl: String) {
    data class PDF(val workers: Int? = 10,
                   val userPassword: String?,
                   val OwnerPassword: String?)
    data class DigitialSigned(val certificate: String?,
                              val password: String?,
                              val reason: String?,
                              val location: String?)
}