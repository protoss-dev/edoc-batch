package th.co.ktaxa.edoc.batch.etaxcert

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import th.co.ktaxa.edoc.batch.etaxcert.utility.DateFormatterUtility

@SpringBootTest
class ExtensionTest {

    @Test
    fun getOrdinalTest() {
        assertThat(getOrdinal(1)).isEqualTo("1st")
        assertThat(getOrdinal(2)).isEqualTo("2nd")
        assertThat(getOrdinal(3)).isEqualTo("3rd")
        assertThat(getOrdinal(4)).isEqualTo("4th")
        assertThat(getOrdinal(11)).isEqualTo("11th")
        assertThat(getOrdinal(12)).isEqualTo("12th")
        assertThat(getOrdinal(13)).isEqualTo("13th")
    }

    @Test
    fun StringToSlug() {
        assertThat("asd\nae-123-+66.pdf".toSlug()).isEqualTo("asd-ae-123-66-pdf")
    }
}