//package th.co.ktaxa.edoc.batch.etaxcert.controller.handling
//
//import com.google.gson.Gson
//import org.keycloak.KeycloakSecurityContext
//
//import th.co.ktaxa.edoc.batch.etaxcert.auth.getSubjectFromToken
//import th.co.ktaxa.edoc.batch.etaxcert.utility.ILogger
//import th.co.ktaxa.edoc.batch.etaxcert.utility.logger
//
//import org.springframework.http.HttpHeaders
//import org.springframework.http.HttpStatus
//import org.springframework.http.ResponseEntity
//import org.springframework.http.converter.HttpMessageNotReadableException
//import org.springframework.web.HttpMediaTypeNotSupportedException
//import org.springframework.web.HttpRequestMethodNotSupportedException
//import org.springframework.web.bind.annotation.ControllerAdvice
//import org.springframework.web.bind.annotation.ExceptionHandler
//import org.springframework.web.context.request.ServletWebRequest
//import org.springframework.web.context.request.WebRequest
//import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
//
//import java.io.PrintWriter
//import java.io.StringWriter
//import java.text.SimpleDateFormat
//import java.util.*
//
//@ControllerAdvice
//class RestExceptionHandler: ResponseEntityExceptionHandler() {
//    companion object : ILogger {
//        //val logger = getLogger(RestExceptionHandler::class.java)
//        val logger = logger()
//        val jsonBuilder = Gson()
//        val sdf8601 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
//    }
//
//    fun doLog(ex: Exception, request: WebRequest, level: String = "info") {
//        val req = (request as ServletWebRequest).request
//        val keyCloakAttributes: KeycloakSecurityContext? = req.getAttribute(
//                KeycloakSecurityContext::class.java.name
//        ) as KeycloakSecurityContext?
//
//        var userName: String?
//        var userEmail: String?
//        var referenceId: String?
//        when(val token = keyCloakAttributes?.token) {
//            null -> {
//                val auth = getSubjectFromToken(req)
//                userName = auth?.name
//                userEmail = auth?.email
//                referenceId = auth?.sub
//            }
//            else -> {
//                userName = token.name
//                userEmail = token.email
//                referenceId = token.subject
//            }
//        }
//
//        val sw = StringWriter()
//        ex.printStackTrace(PrintWriter(sw))
//        val exceptionAsString = sw.toString()
//
//        if (level == "error") {
//            logger.error(jsonBuilder.toJson(mapOf(
//                    "timestamp" to "${sdf8601.format(Date())}",
//                    "reference_id" to referenceId,
//                    "requested_url" to req.requestURL.toString(),
//                    "http_method" to req.method,
//                    "user_name" to userName,
//                    "user_email" to userEmail,
//                    "message" to "APIs throw ${ex.stackTrace}",
//                    "stackTrace" to exceptionAsString
//            )).toString())
//        } else {
//            logger.info(jsonBuilder.toJson(mapOf(
//                    "timestamp" to "${sdf8601.format(Date())}",
//                    "reference_id" to referenceId,
//                    "requested_url" to req.requestURL.toString(),
//                    "http_method" to req.method,
//                    "user_name" to userName,
//                    "user_email" to userEmail,
//                    "message" to "Error throw ${ex.javaClass.name}"
//            )).toString())
//        }
//    }
//
//    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
//        doLog(ex, request)
//        //
//        val error = ex.message.toString()
//        val apiError = ApiError(HttpStatus.BAD_REQUEST, ex.localizedMessage, error)
//        return ResponseEntity(apiError, HttpHeaders(), apiError.status)
//    }
//
//    @ExceptionHandler(MethodArgumentTypeMismatchException::class)
//    fun handleMethodArgumentTypeMismatch(
//            ex: MethodArgumentTypeMismatchException,
//            request: WebRequest
//    ): ResponseEntity<Any> {
//
//        doLog(ex, request)
//        //
//        val error = ex.name + " should be of type " + ex.requiredType!!.name
//
//        val apiError = ApiError(HttpStatus.BAD_REQUEST, ex.localizedMessage, error)
//        return ResponseEntity(apiError, HttpHeaders(), apiError.status)
//    }
//
//    // 405
//    override fun handleHttpRequestMethodNotSupported(
//            ex: HttpRequestMethodNotSupportedException,
//            headers: HttpHeaders,
//            status: HttpStatus,
//            request: WebRequest
//    ): ResponseEntity<Any> {
//
//        doLog(ex, request)
//        //
//        val builder = StringBuilder()
//        builder.append(ex.method)
//        builder.append(" method is not supported for this request. Supported methods are ")
//        ex.supportedHttpMethods!!.forEach { t -> builder.append("$t ") }
//
//        val apiError = ApiError(HttpStatus.METHOD_NOT_ALLOWED, ex.localizedMessage, builder.toString())
//        return ResponseEntity(apiError, HttpHeaders(), apiError.status)
//    }
//
//    // 415
//    override fun handleHttpMediaTypeNotSupported(
//            ex: HttpMediaTypeNotSupportedException,
//            headers: HttpHeaders,
//            status: HttpStatus,
//            request: WebRequest
//    ): ResponseEntity<Any> {
//
//        doLog(ex, request)
//        //
//        val builder = StringBuilder()
//        builder.append(ex.contentType)
//        builder.append(" media type is not supported. Supported media types are ")
//        ex.supportedMediaTypes.forEach { t -> builder.append("$t ") }
//
//        val apiError = ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex.localizedMessage, builder.substring(0, builder.length - 2))
//        return ResponseEntity(apiError, HttpHeaders(), apiError.status)
//    }
//
//    // 500
//    @ExceptionHandler(Exception::class)
//    fun handleAll(ex: Exception, request: WebRequest): ResponseEntity<Any> {
//
//        doLog(ex, request, "error")
//        //
//        val apiError = ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.localizedMessage ?: ex.message ?: "", "error occurred")
//        return ResponseEntity(apiError, HttpHeaders(), apiError.status)
//    }
//}