package th.co.ktaxa.edoc.batch.etaxcert.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal

@SpringBootTest
class TemplateModelTest {

    @Test
    fun initialData() {
        val t = TemplateModel(
                1L,
                "501-223344",
                "501-223344",
                "WPOWNID",
                "WNAME",
                "WADDR1",
                "WADDR2",
                "WADDR3",
                "WADDR4",
                "WCEFF",
                "WISSNAM",
                "WSTRDAT",
                "WENDDAT",
                0,
                "WDUEDT",
                "WPAYDT",
                "WRNO",
                BigDecimal.ONE,
                "WMODE",
                "WACT",
                0,
                0,
                BigDecimal.ONE,
                BigDecimal.ONE,
                BigDecimal.ONE,
                BigDecimal.ONE,
                BigDecimal.TEN,
                1
        )

        assertThat(t).isNotNull
        assertThat(t.ID).isEqualTo(1L)
        assertThat(t.WPNO).isEqualTo("501-223344")
        assertThat(t.WPID).isEqualTo("501-223344")
        assertThat(t.WPOWNID).isEqualTo("WPOWNID")
        assertThat(t.WNAME).isEqualTo("WNAME")
        assertThat(t.WADR1).isEqualTo("WADDR1")
        assertThat(t.WADR2).isEqualTo("WADDR2")
        assertThat(t.WADR3).isEqualTo("WADDR3")
        assertThat(t.WADR4).isEqualTo("WADDR4")
        assertThat(t.WCEFF).isEqualTo("WCEFF")
        assertThat(t.WISSNAM).isEqualTo("WISSNAM")
        assertThat(t.WSTRDAT).isEqualTo("WSTRDAT")
        assertThat(t.WENDDAT).isEqualTo("WENDDAT")
        assertThat(t.WTERM).isEqualTo(0)
        assertThat(t.WDUEDT).isEqualTo("WDUEDT")
        assertThat(t.WPAYDT).isEqualTo("WPAYDT")
        assertThat(t.WRNO).isEqualTo("WRNO")
        assertThat(t.WAMT).isEqualTo(BigDecimal.ONE)
        assertThat(t.WMODE).isEqualTo("WMODE")
        assertThat(t.WACT).isEqualTo("WACT")
        assertThat(t.WPMTH).isEqualTo(0)
        assertThat(t.TRANNO).isEqualTo(0)
        assertThat(t.TAX).isEqualTo(BigDecimal.ONE)
        assertThat(t.NOTAXOTH).isEqualTo(BigDecimal.ONE)
        assertThat(t.NOTAX).isEqualTo(BigDecimal.ONE)
        assertThat(t.HTAX).isEqualTo(BigDecimal.ONE)
        assertThat(t.TOTAMT).isEqualTo(BigDecimal.TEN)
        assertThat(t.TEMPLATE).isEqualTo(1)
    }

}