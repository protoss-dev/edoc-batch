package th.co.ktaxa.edoc.batch.etaxcert.properties

import io.mockk.excludeRecords
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class EtaxcertPropertiesTest {

    @Test
    fun initialData() {
        val etaxcertProperties = EtaxcertProperties(
                "location",
                EtaxcertProperties.PDF(
                        10, "password", "userPassword"
                ),
                EtaxcertProperties.DigitialSigned(
                        "certificate", "password", "no reason", "location"
                ),
                "aws",
                "https://aws.service.com"
        )

        assertThat(etaxcertProperties).isNotNull
        assertThat(etaxcertProperties.location).isEqualTo("location")
        assertThat(etaxcertProperties.pdf).isEqualTo(EtaxcertProperties.PDF(10, "password", "userPassword"))
        assertThat(etaxcertProperties.pdf.workers).isEqualTo(10)
        assertThat(etaxcertProperties.pdf.userPassword).isEqualTo("password")
        assertThat(etaxcertProperties.pdf.OwnerPassword).isEqualTo("userPassword")
        assertThat(etaxcertProperties.digitalSigned).isEqualTo(EtaxcertProperties.DigitialSigned("certificate", "password", "no reason", "location"))
        assertThat(etaxcertProperties.digitalSigned!!.certificate).isEqualTo("certificate")
        assertThat(etaxcertProperties.digitalSigned!!.password).isEqualTo("password")
        assertThat(etaxcertProperties.digitalSigned!!.reason).isEqualTo("no reason")
        assertThat(etaxcertProperties.digitalSigned!!.location).isEqualTo("location")
        assertThat(etaxcertProperties.bucketName).isEqualTo("aws")
        assertThat(etaxcertProperties.pathUrl).isEqualTo("https://aws.service.com")

    }
}