package th.co.ktaxa.edoc.batch.etaxcert.service

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.itextpdf.kernel.geom.Rectangle
import com.itextpdf.kernel.pdf.PdfReader
import com.itextpdf.kernel.pdf.ReaderProperties
import com.itextpdf.kernel.pdf.StampingProperties
import com.itextpdf.signatures.*
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.DigitialSigned
import th.co.ktaxa.edoc.batch.etaxcert.properties.EtaxcertProperties
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.math.BigDecimal
import java.security.KeyStore
import java.security.PrivateKey
import java.security.Security
import java.security.cert.Certificate


abstract class TemplatePdfService(@Autowired private val properties: EtaxcertProperties) : PdfService {

    @Autowired private lateinit var amazonS3 : AmazonS3

    val _logger = LoggerFactory.getLogger(this.javaClass)

    @Value("\${upload-api.url}")
    lateinit var uploadApiUrl: String

    @Throws(IOException::class)
    override fun savePdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String {
        val file = File(dest)
        when (signed) {
            DigitialSigned.SIGNED -> {
                file.outputStream().use {
                    fileOut -> signedPdf(pdfInputStream(taxcerts, password), password, false).copyTo(fileOut)
                }
            }
            DigitialSigned.SIGNED_WITH_APPEARANCE -> {
                file.outputStream().use {
                    fileOut -> signedPdf(pdfInputStream(taxcerts, password), password, true).copyTo(fileOut)
                }
            }
            else -> {
                file.outputStream().use {
                    fileOut -> pdfInputStream(taxcerts, password).copyTo(fileOut)
                }
            }
        }

        return file.toString()
    }

    @Throws(IOException::class)
    override fun generatePdf(taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned): ByteArrayInputStream {

        when (signed) {
            DigitialSigned.SIGNED -> {
                return signedPdf(pdfInputStream(taxcerts, password), password, false)
            }
            DigitialSigned.SIGNED_WITH_APPEARANCE -> {
                return signedPdf(pdfInputStream(taxcerts, password), password, true)
            }
            else -> {
                return pdfInputStream(taxcerts, password)
            }
        }
    }

    @Throws(IOException::class)
    override fun storeApiPdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String {
        val file = File(dest)
        var bis =  when (signed) {
            DigitialSigned.SIGNED -> signedPdf(pdfInputStream(taxcerts, password), password, false)
            DigitialSigned.SIGNED_WITH_APPEARANCE -> signedPdf(pdfInputStream(taxcerts, password), password, true)
            else -> pdfInputStream(taxcerts, password)
        }

        val omd = ObjectMetadata()
        omd.setContentLength(bis.available().toLong())

        amazonS3.putObject(PutObjectRequest(properties.bucketName, properties.pathUrl+"/"+file.name, bis,omd).withCannedAcl(CannedAccessControlList.PublicRead))

        return file.toString()
    }

    @Throws(IOException::class)
    override fun storePdf(dest: String, taxcerts: List<TemplateModel>, password: String?, signed: DigitialSigned) : String {
        /* Connect to API S3 */
        val file = File(dest)
        when (signed) {
            DigitialSigned.SIGNED -> {
                file.outputStream().use {
                    fileOut -> signedPdf(pdfInputStream(taxcerts, password), password, false).copyTo(fileOut)

                }
            }
            DigitialSigned.SIGNED_WITH_APPEARANCE -> {
                file.outputStream().use {
                    fileOut -> signedPdf(pdfInputStream(taxcerts, password), password, true).copyTo(fileOut)
                }
            }
            else -> {
                file.outputStream().use {
                    fileOut -> pdfInputStream(taxcerts, password).copyTo(fileOut)
                }
            }
        }
        var restTemplate: RestTemplate? = RestTemplate()
        val headers = HttpHeaders()
        headers.setContentType(MediaType.MULTIPART_FORM_DATA)
        var body =  LinkedMultiValueMap<String, Object>()

        body.add("fileName",(properties.pathUrl+"/"+file.name) as Object?)
        body.add("file",FileSystemResource(file) as Object?)
        val requestEntity = HttpEntity<MultiValueMap<String, Object>>(body, headers);

        val resultMessage = restTemplate?.postForEntity(uploadApiUrl, requestEntity, String::class.java)?.getBody()

        restTemplate = null

        return file.toString()
    }



    @Throws(IOException::class)
    private fun signedPdf(src: ByteArrayInputStream, password: String?, showDigitalSigned: Boolean? = false) : ByteArrayInputStream {

        val path = properties.digitalSigned?.certificate?.let { ClassPathResource(it).inputStream }
        val pass = properties.digitalSigned?.password?.toCharArray()

        val provider = BouncyCastleProvider()
        Security.addProvider(provider)

        // The first argument defines that the keys and certificates are stored using PKCS#12
        val ks = KeyStore.getInstance("pkcs12", provider.getName())
        ks.load(path , pass)
        val alias = ks.aliases().nextElement()
        val pk: PrivateKey = ks.getKey(alias, pass) as PrivateKey
        val chain = ks.getCertificateChain(alias)

        return Template7PdfServiceImpl(properties).sign(src, password, chain, pk,
                DigestAlgorithms.SHA256, provider.getName(), PdfSigner.CryptoStandard.CMS,
                properties.digitalSigned?.reason!!, properties.digitalSigned?.location!!,
                null, null, null, 0, showDigitalSigned!!);
    }

    fun sign(src: ByteArrayInputStream, password: String?, chain: Array<Certificate>, pk: PrivateKey,
             digestAlgorithm: String, provider: String, subfilter: PdfSigner.CryptoStandard,
             reason: String, location: String, crlList: MutableCollection<ICrlClient>?,
             ocspClient: IOcspClient?, tsaClient: ITSAClient?, estimatedSize: Int, showDigitalSigned: Boolean): ByteArrayInputStream {

        val dest = ByteArrayOutputStream()
        val reader : PdfReader
        if (password != null && password.trim() != "") {
            reader = PdfReader(src, ReaderProperties().setPassword(password!!.toByteArray()));
        }
        else {
            reader = PdfReader(src);
        }
        val signer = PdfSigner(reader, dest, StampingProperties());

        if (showDigitalSigned) {
            // Create the signature appearance
            val rect = Rectangle(480f, 800f, 110f, 40f);
            val appearance = signer.getSignatureAppearance()
            appearance.setReason(reason)
                    .setLocation(location)
                    .setReuseAppearance(false)
                    .setPageRect(rect)
                    .setPageNumber(1);
        }

        signer.setFieldName("sig");

        val pks = PrivateKeySignature(pk, digestAlgorithm, provider);
        val digest = BouncyCastleDigest();

        signer.signDetached(digest, pks, chain, crlList, ocspClient, tsaClient, estimatedSize, subfilter);
        return ByteArrayInputStream(dest.toByteArray())
    }

    fun parenthesesIfNegative( value: BigDecimal): String {

        val valueString = String.format("%,.2f", value)
        return when(value < BigDecimal.ZERO){
            true -> "(${valueString.replace("-","")})"
            false -> valueString
        }
    }

    @Throws(IOException::class)
    abstract fun pdfInputStream(taxcerts: List<TemplateModel>, password: String?): ByteArrayInputStream

}