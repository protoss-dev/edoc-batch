package th.co.ktaxa.edoc.batch.etaxcert.auth

data class Authorization(
        val acr: String,
        val allowedOrigins: List<String>,
        val aud: String,
        val auth_time: Int,
        val azp: String,
        val email: String,
        val exp: Int,
        val family_name: String,
        val given_name: String,
        val iat: Int,
        val iss: String,
        val jti: String,
        val edoc_groups: List<String>,
        val edoc_roles: List<String>,
        val name: String,
        val nbf: Int,
        val nonce: String,
        val preferred_username: String,
        val realm_access: RealmAccess,
        val resource_access: ResourceAccess,
        val session_state: String,
        val sub: String,
        val typ: String
)