package th.co.ktaxa.edoc.batch.etaxcert.service

import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfReader
import com.itextpdf.kernel.pdf.ReaderProperties
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import th.co.ktaxa.edoc.batch.etaxcert.model.TemplateModel
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.DigitialSigned
import java.math.BigDecimal


@SpringBootTest
class Template2PdfServiceTest {

    @Autowired
    lateinit var template2PdfService: Template2PdfServiceImpl

    private val userDirectory = System.getProperty("user.dir")

    @Test
    fun `Generate PDF with Digital Signed Box`() {
        val dest = userDirectory + "/build/tmp/template2-signed-box.pdf"
        val taxcert = TemplateModel(
                ID = 1002682,
                WPNO = "502-0751961",
                WPID = "XXXXXXXXX4879       ",
                WPOWNID = "                    ",
                WNAME = "POWNRF_502-0751961 POWNER_502-0751961",
                WADR1 = "PADR1_502-0751961             ",
                WADR2 = "PADR2_502-0751961             ",
                WADR3 = "PADR3_502-0751961             ",
                WADR4 = "PADR4_502-0751961             ",
                WCEFF = "9 พฤศจิกายน 2552    ",
                WISSNAM = "PNAMF_502-0751961 PNAME_502-0751961",
                WSTRDAT = "1 มกราคม 2562       ",
                WENDDAT = "31 ธันวาคม 2562     ",
                WTERM = 10,
                WDUEDT = "09/06/2562",
                WPAYDT = "10/06/2562",
                WRNO = "A95866179",
                WAMT = BigDecimal(1017.35),
                WMODE = "M",
                WACT = "ATP",
                WPMTH = 1,
                TRANNO = 7,
                TAX = BigDecimal(6663.16),
                NOTAXOTH = BigDecimal(0),
                NOTAX = BigDecimal(0),
                HTAX = BigDecimal(458.29),
                TOTAMT= BigDecimal(7121.45),
                TEMPLATE= 2)

        val taxcerts = mutableListOf(taxcert)
        for (i in 1..30) {
            taxcerts.add(taxcert)
        }
        template2PdfService.savePdf(dest, taxcerts, null, DigitialSigned.SIGNED_WITH_APPEARANCE)
        val pdfDoc = PdfDocument(PdfReader(dest))
        assertEquals(2, pdfDoc.numberOfPages)
    }

    @Test
    fun `Save PDF with Digital Signed`() {
        val dest = userDirectory + "/build/tmp/template2-signed.pdf"
        val taxcert = TemplateModel(
                ID = 1002682,
                WPNO = "502-0751961",
                WPID = "XXXXXXXXX4879       ",
                WPOWNID = "                    ",
                WNAME = "POWNRF_502-0751961 POWNER_502-0751961",
                WADR1 = "PADR1_502-0751961             ",
                WADR2 = "PADR2_502-0751961             ",
                WADR3 = "PADR3_502-0751961             ",
                WADR4 = "PADR4_502-0751961             ",
                WCEFF = "9 พฤศจิกายน 2552    ",
                WISSNAM = "PNAMF_502-0751961 PNAME_502-0751961",
                WSTRDAT = "1 มกราคม 2562       ",
                WENDDAT = "31 ธันวาคม 2562     ",
                WTERM = 10,
                WDUEDT = "09/06/2562",
                WPAYDT = "10/06/2562",
                WRNO = "A95866179",
                WAMT = BigDecimal(1017.35),
                WMODE = "M",
                WACT = "ATP",
                WPMTH = 1,
                TRANNO = 7,
                TAX = BigDecimal(6663.16),
                NOTAXOTH = BigDecimal(0),
                NOTAX = BigDecimal(0),
                HTAX = BigDecimal(458.29),
                TOTAMT= BigDecimal(7121.45),
                TEMPLATE= 2)

        val taxcerts = mutableListOf(taxcert)
        for (i in 1..30) {
            taxcerts.add(taxcert)
        }
        template2PdfService.savePdf(dest, taxcerts, null, DigitialSigned.SIGNED)
        val pdfDoc = PdfDocument(PdfReader(dest))
        assertEquals(2, pdfDoc.numberOfPages)
    }

    @Test
    fun `Generate PDF without Digital Signed`() {
        val dest = userDirectory + "/build/tmp/template2.pdf"
        val taxcert = TemplateModel(
                ID = 1002682,
                WPNO = "502-0751961",
                WPID = "XXXXXXXXX4879       ",
                WPOWNID = "                    ",
                WNAME = "POWNRF_502-0751961 POWNER_502-0751961",
                WADR1 = "PADR1_502-0751961             ",
                WADR2 = "PADR2_502-0751961             ",
                WADR3 = "PADR3_502-0751961             ",
                WADR4 = "PADR4_502-0751961             ",
                WCEFF = "9 พฤศจิกายน 2552    ",
                WISSNAM = "PNAMF_502-0751961 PNAME_502-0751961",
                WSTRDAT = "1 มกราคม 2562       ",
                WENDDAT = "31 ธันวาคม 2562     ",
                WTERM = 10,
                WDUEDT = "09/06/2562",
                WPAYDT = "10/06/2562",
                WRNO = "A95866179",
                WAMT = BigDecimal(1017.35),
                WMODE = "M",
                WACT = "ATP",
                WPMTH = 1,
                TRANNO = 7,
                TAX = BigDecimal(6663.16),
                NOTAXOTH = BigDecimal(0),
                NOTAX = BigDecimal(0),
                HTAX = BigDecimal(458.29),
                TOTAMT= BigDecimal(7121.45),
                TEMPLATE= 2)

        val taxcerts = mutableListOf(taxcert)
        for (i in 1..53) {
            taxcerts.add(taxcert)
        }
        template2PdfService.savePdf(dest, taxcerts, null, DigitialSigned.NO_SIGNED)
        val pdfDoc = PdfDocument(PdfReader(dest))
        assertEquals(3, pdfDoc.numberOfPages)
    }

    @Test
    fun `Generate PDF`() {
        val taxcert = TemplateModel(
                ID = 1002682,
                WPNO = "502-0751961",
                WPID = "XXXXXXXXX4879       ",
                WPOWNID = "                    ",
                WNAME = "POWNRF_502-0751961 POWNER_502-0751961",
                WADR1 = "PADR1_502-0751961             ",
                WADR2 = "PADR2_502-0751961             ",
                WADR3 = "PADR3_502-0751961             ",
                WADR4 = "PADR4_502-0751961             ",
                WCEFF = "9 พฤศจิกายน 2552    ",
                WISSNAM = "PNAMF_502-0751961 PNAME_502-0751961",
                WSTRDAT = "1 มกราคม 2562       ",
                WENDDAT = "31 ธันวาคม 2562     ",
                WTERM = 10,
                WDUEDT = "09/06/2562",
                WPAYDT = "10/06/2562",
                WRNO = "A95866179",
                WAMT = BigDecimal(1017.35),
                WMODE = "M",
                WACT = "ATP",
                WPMTH = 1,
                TRANNO = 7,
                TAX = BigDecimal(6663.16),
                NOTAXOTH = BigDecimal(0),
                NOTAX = BigDecimal(0),
                HTAX = BigDecimal(458.29),
                TOTAMT= BigDecimal(7121.45),
                TEMPLATE= 2)

        val taxcerts = listOf(taxcert)
        val byteArrayInputStream = template2PdfService.generatePdf(taxcerts, null, DigitialSigned.NO_SIGNED)
        val pdfDoc = PdfDocument(PdfReader(byteArrayInputStream))
        assertEquals(1, pdfDoc.numberOfPages)


        val taxcerts14 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream14 = template2PdfService.generatePdf(taxcerts14, null, DigitialSigned.NO_SIGNED)
        val pdfDoc14 = PdfDocument(PdfReader(byteArrayInputStream14))
        assertEquals(1, pdfDoc14.numberOfPages)

        val taxcerts15 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream15 = template2PdfService.generatePdf(taxcerts15, null, DigitialSigned.NO_SIGNED)
        val pdfDoc15 = PdfDocument(PdfReader(byteArrayInputStream15))
        assertEquals(2, pdfDoc15.numberOfPages)

        val taxcerts16 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert,
                taxcert)
        val byteArrayInputStream16 = template2PdfService.generatePdf(taxcerts16, null, DigitialSigned.NO_SIGNED)
        val pdfDoc16 = PdfDocument(PdfReader(byteArrayInputStream16))
        assertEquals(2, pdfDoc16.numberOfPages)

        val taxcerts17 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert,
                taxcert, taxcert)
        val byteArrayInputStream17 = template2PdfService.generatePdf(taxcerts17, null, DigitialSigned.NO_SIGNED)
        val pdfDoc17 = PdfDocument(PdfReader(byteArrayInputStream17))
        assertEquals(2, pdfDoc17.numberOfPages)

        val taxcerts18 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert,
                taxcert, taxcert, taxcert)
        val byteArrayInputStream18 = template2PdfService.generatePdf(taxcerts18, null, DigitialSigned.NO_SIGNED)
        val pdfDoc18 = PdfDocument(PdfReader(byteArrayInputStream18))
        assertEquals(2, pdfDoc18.numberOfPages)

        val taxcerts19 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert,
                taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream19 = template2PdfService.generatePdf(taxcerts19, null, DigitialSigned.NO_SIGNED)
        val pdfDoc19 = PdfDocument(PdfReader(byteArrayInputStream19))
        assertEquals(2, pdfDoc19.numberOfPages)


        val taxcerts20 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream20 = template2PdfService.generatePdf(taxcerts20, null, DigitialSigned.NO_SIGNED)
        val pdfDoc20 = PdfDocument(PdfReader(byteArrayInputStream20))
        assertEquals(2, pdfDoc20.numberOfPages)

        val taxcerts21 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert)
        val byteArrayInputStream21 = template2PdfService.generatePdf(taxcerts21, null, DigitialSigned.NO_SIGNED)
        val pdfDoc21 = PdfDocument(PdfReader(byteArrayInputStream21))
        assertEquals(2, pdfDoc21.numberOfPages)

        val taxcerts22 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert)
        val byteArrayInputStream22 = template2PdfService.generatePdf(taxcerts22, null, DigitialSigned.NO_SIGNED)
        val pdfDoc22 = PdfDocument(PdfReader(byteArrayInputStream22))
        assertEquals(2, pdfDoc22.numberOfPages)

        val taxcerts23 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert)
        val byteArrayInputStream23 = template2PdfService.generatePdf(taxcerts23, null, DigitialSigned.NO_SIGNED)
        val pdfDoc23 = PdfDocument(PdfReader(byteArrayInputStream23))
        assertEquals(2, pdfDoc23.numberOfPages)

        val taxcerts24 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream24 = template2PdfService.generatePdf(taxcerts24, null, DigitialSigned.NO_SIGNED)
        val pdfDoc24 = PdfDocument(PdfReader(byteArrayInputStream24))
        assertEquals(2, pdfDoc24.numberOfPages)

        val taxcerts25 = listOf(
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert ,taxcert,
                taxcert, taxcert, taxcert, taxcert, taxcert)
        val byteArrayInputStream25 = template2PdfService.generatePdf(taxcerts25, null, DigitialSigned.NO_SIGNED)
        val pdfDoc25 = PdfDocument(PdfReader(byteArrayInputStream25))
        assertEquals(2, pdfDoc25.numberOfPages)

        val taxcerts26 = ArrayList<TemplateModel>()
        for(x in 0..26)
            taxcerts26.add(taxcert)
        val byteArrayInputStream26 = template2PdfService.generatePdf(taxcerts26, null, DigitialSigned.NO_SIGNED)
        val pdfDoc26 = PdfDocument(PdfReader(byteArrayInputStream26))
        assertEquals(2, pdfDoc26.numberOfPages)

        val taxcerts27 = ArrayList<TemplateModel>()
        for(x in 0..27)
            taxcerts27.add(taxcert)
        val byteArrayInputStream27 = template2PdfService.generatePdf(taxcerts27, null, DigitialSigned.NO_SIGNED)
        val pdfDoc27 = PdfDocument(PdfReader(byteArrayInputStream27))
        assertEquals(2, pdfDoc27.numberOfPages)

        val taxcerts28 = ArrayList<TemplateModel>()
        for(x in 0..28)
            taxcerts28.add(taxcert)
        val byteArrayInputStream28 = template2PdfService.generatePdf(taxcerts28, null, DigitialSigned.NO_SIGNED)
        val pdfDoc28 = PdfDocument(PdfReader(byteArrayInputStream28))
        assertEquals(2, pdfDoc28.numberOfPages)

        val taxcerts29 = ArrayList<TemplateModel>()
        for(x in 0..29)
            taxcerts29.add(taxcert)
        val byteArrayInputStream29 = template2PdfService.generatePdf(taxcerts29, null, DigitialSigned.NO_SIGNED)
        val pdfDoc29 = PdfDocument(PdfReader(byteArrayInputStream29))
        assertEquals(2, pdfDoc29.numberOfPages)

        val taxcerts30 = ArrayList<TemplateModel>()
        for(x in 0..30)
            taxcerts30.add(taxcert)
        val byteArrayInputStream30 = template2PdfService.generatePdf(taxcerts30, null, DigitialSigned.NO_SIGNED)
        val pdfDoc30 = PdfDocument(PdfReader(byteArrayInputStream30))
        assertEquals(2, pdfDoc30.numberOfPages)

        val taxcerts31 = ArrayList<TemplateModel>()
        for(x in 0..31)
            taxcerts31.add(taxcert)
        val byteArrayInputStream31 = template2PdfService.generatePdf(taxcerts31, null, DigitialSigned.NO_SIGNED)
        val pdfDoc31 = PdfDocument(PdfReader(byteArrayInputStream31))
        assertEquals(2, pdfDoc31.numberOfPages)

        val taxcerts32 = ArrayList<TemplateModel>()
        for(x in 0..32)
            taxcerts32.add(taxcert)
        val byteArrayInputStream32 = template2PdfService.generatePdf(taxcerts32, null, DigitialSigned.NO_SIGNED)
        val pdfDoc32 = PdfDocument(PdfReader(byteArrayInputStream32))
        assertEquals(2, pdfDoc32.numberOfPages)

        val taxcerts33 = ArrayList<TemplateModel>()
        for(x in 0..33)
            taxcerts33.add(taxcert)
        val byteArrayInputStream33 = template2PdfService.generatePdf(taxcerts33, null, DigitialSigned.NO_SIGNED)
        val pdfDoc33 = PdfDocument(PdfReader(byteArrayInputStream33))
        assertEquals(2, pdfDoc33.numberOfPages)

        val taxcerts49 = ArrayList<TemplateModel>()
        for(x in 0..50)
            taxcerts49.add(taxcert)
        val byteArrayInputStream49 = template2PdfService.generatePdf(taxcerts49, null, DigitialSigned.NO_SIGNED)
        val pdfDoc49 = PdfDocument(PdfReader(byteArrayInputStream49))
        assertEquals(2, pdfDoc49.numberOfPages)

        val taxcerts50 = ArrayList<TemplateModel>()
        for(x in 0..50)
            taxcerts50.add(taxcert)
        val byteArrayInputStream50 = template2PdfService.generatePdf(taxcerts50, null, DigitialSigned.NO_SIGNED)
        val pdfDoc50 = PdfDocument(PdfReader(byteArrayInputStream50))
        assertEquals(2, pdfDoc50.numberOfPages)

        val taxcerts51 = ArrayList<TemplateModel>()
        for(x in 0..51)
            taxcerts51.add(taxcert)
        val byteArrayInputStream51 = template2PdfService.generatePdf(taxcerts51, null, DigitialSigned.NO_SIGNED)
        val pdfDoc51 = PdfDocument(PdfReader(byteArrayInputStream51))
        assertEquals(3, pdfDoc51.numberOfPages)

        val taxcerts52 = ArrayList<TemplateModel>()
        for(x in 0..52)
            taxcerts52.add(taxcert)
        val byteArrayInputStream52 = template2PdfService.generatePdf(taxcerts52, null, DigitialSigned.NO_SIGNED)
        val pdfDoc52 = PdfDocument(PdfReader(byteArrayInputStream52))
        assertEquals(3, pdfDoc52.numberOfPages)

        val taxcerts53 = ArrayList<TemplateModel>()
        for(x in 0..53)
            taxcerts53.add(taxcert)
        val byteArrayInputStream53 = template2PdfService.generatePdf(taxcerts53, null, DigitialSigned.NO_SIGNED)
        val pdfDoc53 = PdfDocument(PdfReader(byteArrayInputStream53))
        assertEquals(3, pdfDoc53.numberOfPages)

        val taxcerts54 = ArrayList<TemplateModel>()
        for(x in 0..54)
            taxcerts54.add(taxcert)
        val byteArrayInputStream54 = template2PdfService.generatePdf(taxcerts54, null, DigitialSigned.NO_SIGNED)
        val pdfDoc54 = PdfDocument(PdfReader(byteArrayInputStream54))
        assertEquals(3, pdfDoc54.numberOfPages)

        val taxcerts55 = ArrayList<TemplateModel>()
        for(x in 0..55)
            taxcerts55.add(taxcert)
        val byteArrayInputStream55 = template2PdfService.generatePdf(taxcerts55, null, DigitialSigned.NO_SIGNED)
        val pdfDoc55 = PdfDocument(PdfReader(byteArrayInputStream55))
        assertEquals(3, pdfDoc55.numberOfPages)

        val taxcerts56 = ArrayList<TemplateModel>()
        for(x in 0..56)
            taxcerts56.add(taxcert)
        val byteArrayInputStream56 = template2PdfService.generatePdf(taxcerts56, null, DigitialSigned.NO_SIGNED)
        val pdfDoc56 = PdfDocument(PdfReader(byteArrayInputStream56))
        assertEquals(3, pdfDoc56.numberOfPages)

        val taxcerts57 = ArrayList<TemplateModel>()
        for(x in 0..57)
            taxcerts57.add(taxcert)
        val byteArrayInputStream57 = template2PdfService.generatePdf(taxcerts57, null, DigitialSigned.NO_SIGNED)
        val pdfDoc57 = PdfDocument(PdfReader(byteArrayInputStream57))
        assertEquals(3, pdfDoc57.numberOfPages)

        val taxcerts58 = ArrayList<TemplateModel>()
        for(x in 0..58)
            taxcerts58.add(taxcert)
        val byteArrayInputStream58 = template2PdfService.generatePdf(taxcerts58, null, DigitialSigned.NO_SIGNED)
        val pdfDoc58 = PdfDocument(PdfReader(byteArrayInputStream58))
        assertEquals(3, pdfDoc58.numberOfPages)

        val taxcerts59 = ArrayList<TemplateModel>()
        for(x in 0..58)
            taxcerts59.add(taxcert)
        val byteArrayInputStream59 = template2PdfService.generatePdf(taxcerts59, null, DigitialSigned.NO_SIGNED)
        val pdfDoc59 = PdfDocument(PdfReader(byteArrayInputStream59))
        assertEquals(3, pdfDoc59.numberOfPages)

        val taxcerts60 = ArrayList<TemplateModel>()
        for(x in 0..60)
            taxcerts60.add(taxcert)
        val byteArrayInputStream60 = template2PdfService.generatePdf(taxcerts60, null, DigitialSigned.NO_SIGNED)
        val pdfDoc60 = PdfDocument(PdfReader(byteArrayInputStream60))
        assertEquals(3, pdfDoc60.numberOfPages)

        val taxcerts61 = ArrayList<TemplateModel>()
        for(x in 0..61)
            taxcerts61.add(taxcert)
        val byteArrayInputStream61 = template2PdfService.generatePdf(taxcerts61, null, DigitialSigned.NO_SIGNED)
        val pdfDoc61 = PdfDocument(PdfReader(byteArrayInputStream61))
        assertEquals(3, pdfDoc61.numberOfPages)

        val taxcerts62 = ArrayList<TemplateModel>()
        for(x in 0..62)
            taxcerts62.add(taxcert)
        val byteArrayInputStream62 = template2PdfService.generatePdf(taxcerts62, null, DigitialSigned.NO_SIGNED)
        val pdfDoc62 = PdfDocument(PdfReader(byteArrayInputStream62))
        assertEquals(3, pdfDoc62.numberOfPages)

        val taxcerts63 = ArrayList<TemplateModel>()
        for(x in 0..63)
            taxcerts63.add(taxcert)
        val byteArrayInputStream63 = template2PdfService.generatePdf(taxcerts63, null, DigitialSigned.NO_SIGNED)
        val pdfDoc63 = PdfDocument(PdfReader(byteArrayInputStream63))
        assertEquals(3, pdfDoc63.numberOfPages)

        val taxcerts64 = ArrayList<TemplateModel>()
        for(x in 0..61)
            taxcerts64.add(taxcert)
        val byteArrayInputStream64 = template2PdfService.generatePdf(taxcerts64, null, DigitialSigned.NO_SIGNED)
        val pdfDoc64 = PdfDocument(PdfReader(byteArrayInputStream64))
        assertEquals(3, pdfDoc64.numberOfPages)

        val taxcerts65 = ArrayList<TemplateModel>()
        for(x in 0..65)
            taxcerts65.add(taxcert)
        val byteArrayInputStream65 = template2PdfService.generatePdf(taxcerts65, null, DigitialSigned.NO_SIGNED)
        val pdfDoc65 = PdfDocument(PdfReader(byteArrayInputStream65))
        assertEquals(3, pdfDoc65.numberOfPages)
    }

    @Test
    fun `Generate PDF with Password`() {
        val taxcert = TemplateModel(
                ID = 1002682,
                WPNO = "502-0751961",
                WPID = "XXXXXXXXX4879       ",
                WPOWNID = "                    ",
                WNAME = "POWNRF_502-0751961 POWNER_502-0751961",
                WADR1 = "PADR1_502-0751961             ",
                WADR2 = "PADR2_502-0751961             ",
                WADR3 = "PADR3_502-0751961             ",
                WADR4 = "PADR4_502-0751961             ",
                WCEFF = "9 พฤศจิกายน 2552    ",
                WISSNAM = "PNAMF_502-0751961 PNAME_502-0751961",
                WSTRDAT = "1 มกราคม 2562       ",
                WENDDAT = "31 ธันวาคม 2562     ",
                WTERM = 10,
                WDUEDT = "09/06/2562",
                WPAYDT = "10/06/2562",
                WRNO = "A95866179",
                WAMT = BigDecimal(1017.35),
                WMODE = "M",
                WACT = "ATP",
                WPMTH = 1,
                TRANNO = 7,
                TAX = BigDecimal(6663.16),
                NOTAXOTH = BigDecimal(0),
                NOTAX = BigDecimal(0),
                HTAX = BigDecimal(458.29),
                TOTAMT= BigDecimal(7121.45),
                TEMPLATE= 2)

        val taxcerts = listOf(taxcert)
        val byteArrayInputStream = template2PdfService.generatePdf(taxcerts, "1234", DigitialSigned.NO_SIGNED)
        val pdfDoc = PdfDocument(PdfReader(byteArrayInputStream, ReaderProperties().setPassword("1234".toByteArray())))
        assertEquals(1, pdfDoc.numberOfPages)
    }
}