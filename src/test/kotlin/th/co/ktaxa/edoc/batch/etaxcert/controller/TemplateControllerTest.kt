package th.co.ktaxa.edoc.batch.etaxcert.controller

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.hasItem
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

@SpringBootTest
@AutoConfigureMockMvc
class TemplateControllerTest() {

    @Autowired
    private lateinit var templateController: TemplateController

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun loadContext() {
        assertThat(templateController).isNotNull
        assertThat(templateController.taxcertRepo).isNotNull
        assertThat(templateController.template1PdfService).isNotNull
        assertThat(templateController.template2PdfService).isNotNull
        assertThat(templateController.template3PdfService).isNotNull
        assertThat(templateController.template4PdfService).isNotNull
        assertThat(templateController.template5PdfService).isNotNull
        assertThat(templateController.template6PdfService).isNotNull
        assertThat(templateController.template7PdfService).isNotNull
        assertThat(templateController.startTimeMetric).isNotNull
        assertThat(templateController.generatedPolicyMetric).isNotNull
        assertThat(templateController.allPolicyMetric).isNotNull
    }

    @Test
    fun generateByPolicyWithoutPolicyNo() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo="))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun generateByPolicyWithoutPolicyNoInDB() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=XXX-1234567"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.success", `is`(false)))
                .andExpect(jsonPath("$.message", `is`("No data found to generate Policy No. XXX-1234567")))
//                .andExpect(jsonPath("$.errors").isArray)
                .andExpect(jsonPath("$.data", `is`("No data found to generate Policy No. XXX-1234567")))
    }

    @Test
    fun generateByPolicyTemplate1() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006169"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 1\""))
    }

    @Test
    fun generateByPolicyTemplate2() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006170"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 2\""))
    }

    @Test
    fun generateByPolicyTemplate3() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006171"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 3\""))
    }

    @Test
    fun generateByPolicyTemplate4() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006172"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 4\""))
    }

    @Test
    fun generateByPolicyTemplate5() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006173"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 5\""))
    }

    @Test
    fun generateByPolicyTemplate6() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006174"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 6\""))
    }

    @Test
    fun generateByPolicyTemplate7() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006175"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Done Template : 7\""))
    }

    @Test
    fun generateByPolicyUnknownTemplate() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/gen?policyNo=501-1006176"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json("\"Unknown template\""))
    }

    @Test
    fun generateAllThread() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/batch-api/etaxcert/all"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
    }
}