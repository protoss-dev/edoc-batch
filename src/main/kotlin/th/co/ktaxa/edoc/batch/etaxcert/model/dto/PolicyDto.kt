package th.co.ktaxa.edoc.batch.etaxcert.model.dto

import com.google.gson.annotations.SerializedName


data class PolicyDto (

        @SerializedName("WPNO")var WPNO: String?,
        @SerializedName("TEMPLATE")var TEMPLATE: Int?
)