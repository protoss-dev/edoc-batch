package th.co.ktaxa.edoc.batch.etaxcert.auth

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class AccountTest {

    @Test
    fun initialData() {
        val a = Account(listOf("user","admin"))

        assertThat(a.roles).isEqualTo(listOf("user","admin"))
    }

}