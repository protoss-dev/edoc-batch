package th.co.ktaxa.edoc.batch.etaxcert.auth

data class Account(
        val roles: List<String>
)