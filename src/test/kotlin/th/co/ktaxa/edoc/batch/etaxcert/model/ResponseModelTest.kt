package th.co.ktaxa.edoc.batch.etaxcert.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ResponseModelTest {

    @Test
    fun initialData() {
        val responseModel = ResponseModel(true, "test message", null, "data")
        assertThat(responseModel).isNotNull
        assertThat(responseModel).isEqualTo(ResponseModel(true, "test message", null, "data"))
        assertThat(responseModel.success).isEqualTo(true)
        assertThat(responseModel.message).isEqualTo("test message")
        assertThat(responseModel.errorCode).isNull()
        assertThat(responseModel.data).isEqualTo("data")
    }
}