# shellcheck disable=SC2034
# shellcheck disable=SC2006

# Todo : Build  Artifact
echo "------------------- Build Artifact -------------------"
gradle clean build -x test

# Todo : version of Project
echo "------------------- Get version -------------------"
version=`gradle properties -q | grep "version:" | awk '/^version/'`
version=`echo "$version" | grep 'version: ' | awk '{print \$2}'`
echo "App version - $version"

# Todo : Build Docker Images
echo "------------------- Build Docker Images -------------------"
docker build -t protosstechnology/001-etc-gen-file-api:"$version" .
# Todo : Tag version images
echo "------------- Docker images version $version --------------"
docker tag protosstechnology/001-etc-gen-file-api:"$version" protosstechnology/001-etc-gen-file-api:"$version"
docker tag protosstechnology/001-etc-gen-file-api:"$version" protosstechnology/001-etc-gen-file-api:latest
# Todo : Push docker images
echo "------------- Push Docker images version: $version --------------"
docker push protosstechnology/001-etc-gen-file-api:"$version"
docker push protosstechnology/001-etc-gen-file-api:latest
echo "------------- complete --------------"




