//package th.co.ktaxa.edoc.batch.etaxcert.utility
//
//import org.slf4j.Logger
//import org.slf4j.LoggerFactory
//import kotlin.reflect.full.companionObject
//
//inline fun <T : Any> getClassForLogging(javaClass: Class<T>): Class<*> {
//    return javaClass.enclosingClass?.takeIf {
//        it.kotlin.companionObject?.java == javaClass
//    } ?: javaClass
//}
//
//fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)
//
//inline fun <reified T : ILogger> T.logger(): Logger
//        = getLogger(getClassForLogging(T::class.java))
//
//fun <T : ILogger> T.logger(classObj: Class<*>): Logger = getLogger(classObj)