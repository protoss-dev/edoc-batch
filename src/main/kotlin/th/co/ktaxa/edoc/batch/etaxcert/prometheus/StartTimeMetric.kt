package th.co.ktaxa.edoc.batch.etaxcert.prometheus

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import org.springframework.stereotype.Component;

@Component
class StartTimeMetric :  MeterBinder {

    private var value = 0L

    fun value(): Long {
        return value
    }

    fun setVal(value: Long){
        this.value = value;
    }
    
    override fun bindTo(registry:MeterRegistry) {
        registry?.gauge("batch_gen_taxcert_start_time", this, { this.value().toDouble() })
    }


    

}