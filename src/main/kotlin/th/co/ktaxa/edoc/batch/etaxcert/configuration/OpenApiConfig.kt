package th.co.ktaxa.edoc.batch.etaxcert.configuration

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {

    @Bean
    fun customOpenAPI(): OpenAPI {
        return OpenAPI()
                .components(Components())
                .info(Info().title("Batch API").description(
                        "This is a Batch API service using SpringDoc-Open Api and OpenAPI 3."))
    }

}