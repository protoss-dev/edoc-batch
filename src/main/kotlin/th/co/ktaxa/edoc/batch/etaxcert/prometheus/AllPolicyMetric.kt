package th.co.ktaxa.edoc.batch.etaxcert.prometheus

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import org.springframework.stereotype.Component;

@Component
class AllPolicyMetric :  MeterBinder {

    private var value = 0

    fun value(): Int {
        return value
    }

    fun setVal(value: Int){
        this.value = value;
    }
    
    override fun bindTo(registry:MeterRegistry) {
        registry?.gauge("batch_gen_taxcert_all_policy_count", this, { this.value().toDouble() })
    }


    

}