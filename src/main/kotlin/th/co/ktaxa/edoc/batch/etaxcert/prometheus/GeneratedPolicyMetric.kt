package th.co.ktaxa.edoc.batch.etaxcert.prometheus

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import org.springframework.stereotype.Component;

@Component
class GeneratedPolicyMetric :  MeterBinder {

    private var value = 0

    fun value(): Int {
        return value
    }

    fun addVal(value: Int){
        this.value = this.value +value;
    }
    
    override fun bindTo(registry:MeterRegistry) {
        registry?.gauge("batch_gen_taxcert_generated_policy_count", this, { this.value().toDouble() })
    }


    

}