FROM gradle:5.4.1-jdk8

USER root

ADD . .

RUN [ "gradle", "build", "--scan" ]

VOLUME /tmp

ENV JAVA_OPTS="-Xmx2g"

      #CMD ["/bin/bash"]

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar $APP_OPTS /home/gradle/build/libs/kaldis-etl-0.0.1-SNAPSHOT.jar", "-d" ]