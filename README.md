# Gen File

Kotlin + Upload to S3

## Change profile environment
Change data spring.profiles.active in file application.yml like this below 
```
spring:   
  profiles:   
    active: dev 
```

## Build project
```shell script
gradle build
```

## Run project
```shell script
gradle bootRun   
```  

## Url for project
```
http://localhost:9002
```

## Post export url
### When request done, file will save at path /tmp
```shell script
http://localhost:9002/batch-api/etaxcert/gen?policyNo=501-1007613
```
## Build Image
```shell script
docker build . -t protosstechnology/001-etc-gen-file-api
```
## Push Image
```shell script
docker push protosstechnology/001-etc-gen-file-api
```
