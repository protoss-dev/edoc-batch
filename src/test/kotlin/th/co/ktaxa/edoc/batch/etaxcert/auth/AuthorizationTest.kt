package th.co.ktaxa.edoc.batch.etaxcert.auth

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class AuthorizationTest {

    @Test
    fun initData() {
        val o = Authorization(
                "acr",
                arrayListOf("allowedOrigins"),
                "aud",
                123456789,
                "azp",
                "edoc@mail.com",
                987654321,
                "family name",
                "given name",
                111,
                "iss",
                "jti",
                arrayListOf("groups"),
                arrayListOf("roles"),
                "name",
                222,
                "nonce",
                "username",
                RealmAccess(arrayListOf("user")),
                ResourceAccess(Account(arrayListOf("user"))),
                "session state",
                "sub",
                "type"
        );

        assertThat(o.acr).isEqualTo("acr")
        assertThat(o.allowedOrigins).isEqualTo(arrayListOf("allowedOrigins"))
        assertThat(o.aud).isEqualTo("aud")
        assertThat(o.auth_time).isEqualTo(123456789)
        assertThat(o.azp).isEqualTo("azp")
        assertThat(o.email).isEqualTo("edoc@mail.com")
        assertThat(o.exp).isEqualTo(987654321)
        assertThat(o.family_name).isEqualTo("family name")
        assertThat(o.given_name).isEqualTo("given name")
        assertThat(o.iat).isEqualTo(111)
        assertThat(o.iss).isEqualTo("iss")
        assertThat(o.jti).isEqualTo("jti")
        assertThat(o.edoc_groups).isEqualTo(arrayListOf("groups"))
        assertThat(o.edoc_roles).isEqualTo(arrayListOf("roles"))
        assertThat(o.name).isEqualTo("name")
        assertThat(o.nbf).isEqualTo(222)
        assertThat(o.nonce).isEqualTo("nonce")
        assertThat(o.preferred_username).isEqualTo("username")
        assertThat(o.realm_access).isEqualTo(RealmAccess(arrayListOf("user")))
        assertThat(o.resource_access).isEqualTo(ResourceAccess(Account(arrayListOf("user"))))
        assertThat(o.session_state).isEqualTo("session state")
        assertThat(o.sub).isEqualTo("sub")
        assertThat(o.typ).isEqualTo("type")
    }
}