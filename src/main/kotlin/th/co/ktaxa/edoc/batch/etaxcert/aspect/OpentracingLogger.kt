package th.co.ktaxa.edoc.batch.etaxcert.aspect

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class OpentracingLogger