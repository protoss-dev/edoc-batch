package th.co.ktaxa.edoc.batch.etaxcert.auth

import com.google.gson.Gson
import java.util.*
import javax.servlet.http.HttpServletRequest

fun getSubjectFromToken(request: HttpServletRequest): Authorization? {
    val jsonBuilder = Gson()
    return try {
        jsonBuilder.fromJson(
                String(
                        Base64.getDecoder().decode(
                        request.getHeader("authorization")
                                .replace("Bearer ", "")
                                .split("\\.".toRegex())[1]
                )),
                Authorization::class.java
        )
    } catch (e: Throwable) {
        null
    }
}