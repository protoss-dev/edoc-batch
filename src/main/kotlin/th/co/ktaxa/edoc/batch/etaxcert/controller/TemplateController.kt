package th.co.ktaxa.edoc.batch.etaxcert.controller

import com.google.gson.Gson
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import th.co.ktaxa.edoc.batch.etaxcert.model.ResponseModel
import th.co.ktaxa.edoc.batch.etaxcert.model.dto.PolicyDto
import th.co.ktaxa.edoc.batch.etaxcert.model.enum.DigitialSigned
import th.co.ktaxa.edoc.batch.etaxcert.properties.EtaxcertProperties
import th.co.ktaxa.edoc.batch.etaxcert.repository.TaxcertRepository
import th.co.ktaxa.edoc.batch.etaxcert.service.*
import th.co.ktaxa.edoc.batch.etaxcert.worker.TemplateWorkerThread
import java.util.concurrent.Executors
import javax.servlet.http.HttpServletRequest
import kotlin.system.measureTimeMillis
import th.co.ktaxa.edoc.batch.etaxcert.prometheus.*
import java.util.Date


@RestController
@RequestMapping("/batch-api/etaxcert")
@Tag(name = "Template", description = "Template's API")
class TemplateController (private val properties: EtaxcertProperties) {

	private val request: HttpServletRequest? = null

	@Autowired lateinit var taxcertRepo: TaxcertRepository

	@Autowired lateinit var template1PdfService: Template1PdfServiceImpl
	@Autowired lateinit var template2PdfService: Template2PdfServiceImpl
	@Autowired lateinit var template3PdfService: Template3PdfServiceImpl
	@Autowired lateinit var template4PdfService: Template4PdfServiceImpl
	@Autowired lateinit var template5PdfService: Template5PdfServiceImpl
	@Autowired lateinit var template6PdfService: Template6PdfServiceImpl
	@Autowired lateinit var template7PdfService: Template7PdfServiceImpl
	@Autowired lateinit var startTimeMetric: StartTimeMetric
	@Autowired lateinit var generatedPolicyMetric: GeneratedPolicyMetric
	@Autowired lateinit var allPolicyMetric: AllPolicyMetric

	@Operation(summary = "Generate by policy", description = "Generate Template by policy", tags = ["Template"])
	@ApiResponses(value =  [ApiResponse(responseCode = "200", description = "successful operation")])
	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/gen", produces = ["application/json"])
	fun generateByPolicy(@Parameter(description = "Policy No.") @RequestParam(value="policyNo") policyNo: String): ResponseEntity<String> {

		val headers = HttpHeaders()
		headers.contentType = MediaType.APPLICATION_JSON
		

			var result: String
			if (policyNo.trim() == "") {
				result = "Required policy no."
			} else {
				var taxcerts = taxcertRepo.findByWPNO(policyNo)
				if (taxcerts.size > 0) {
					when (taxcerts[0].TEMPLATE) {
						1 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template1PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							template1PdfService.storePdf(file, taxcerts, null, DigitialSigned.NO_SIGNED)
							template1PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED_WITH_APPEARANCE)
							template1PdfService.storeApiPdf(file, taxcerts, null, DigitialSigned.SIGNED)
							template1PdfService.storeApiPdf(file, taxcerts, null, DigitialSigned.NO_SIGNED)
							template1PdfService.storeApiPdf(file, taxcerts, null, DigitialSigned.SIGNED_WITH_APPEARANCE)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						2 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template2PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						3 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template3PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						4 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template4PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						5 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template5PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						6 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template6PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						7 -> {
							val file : String = properties.location + "/" + taxcerts[0].WPNO + ".pdf"
							template7PdfService.storePdf(file, taxcerts, null, DigitialSigned.SIGNED)
							result = "Done Template : " + taxcerts[0].TEMPLATE
						}
						else -> {
							result = "Unknown template"
						}
					}
				}
				else {
					val responseModel = ResponseModel(false, "No data found to generate Policy No. ${policyNo}", null, "No data found to generate Policy No. ${policyNo}")
					return ResponseEntity
							.ok()
							.headers(headers)
							.body(Gson().toJson(responseModel))
//					throw Exception("No data found to generate Policy No. ${policyNo}")
				}
			}
			return ResponseEntity
					.ok()
					.headers(headers)
					.body(Gson().toJson(result))
	}

	@Operation(summary = "Generate all thread", description = "Generate all thread", tags = ["Template"])
	@ApiResponses(value =  [ApiResponse(responseCode = "200", description = "successful operation")])
	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/all", produces = ["application/json"])
	fun generateAllThread(): ResponseEntity<String> {

		val headers = HttpHeaders()
		headers.contentType = MediaType.APPLICATION_JSON

		//Stamp Start Time to Prometheus
        startTimeMetric.setVal(Date().getTime())

		try {
			var result: String

			val listPolicy : List<PolicyDto> = taxcertRepo.findAllPolicy()

			//Stamp All Policy to Prometheus
			allPolicyMetric.setVal(listPolicy.size)

			val executor = Executors.newFixedThreadPool(properties.pdf.workers!!.toInt())
			val t = measureTimeMillis {
				for (policy in listPolicy) {

					var taxcerts = taxcertRepo.findByWPNO(policy.WPNO!!)
					var pdfService: PdfService = template6PdfService
					when (policy.TEMPLATE) {
						1 -> pdfService = template1PdfService
						2 -> pdfService = template2PdfService
						3 -> pdfService = template3PdfService
						4 -> pdfService = template4PdfService
						5 -> pdfService = template5PdfService
						6 -> pdfService = template6PdfService
						7 -> pdfService = template7PdfService
					}
					var worker: Runnable = TemplateWorkerThread(taxcerts, pdfService, properties)
					executor.execute(worker)

					generatedPolicyMetric.addVal(properties.pdf.workers!!.toInt())
				}
				executor.shutdown()
				/*
				while (!executor.isTerminated) {}
				*/
			}
			result = "Completed in ${t} ms"

			return ResponseEntity
					.ok()
					.headers(headers)
					.body(Gson().toJson(result))
		}
		catch (ex: Exception) {
			return ResponseEntity
					.badRequest()
					.headers(headers)
					.body(Gson().toJson(ex))

		}
	}

	/*
	@PostMapping("/all")
	fun generateAllCoroutine(): ResponseEntity<String> {

		val headers = HttpHeaders()
		headers.contentType = MediaType.APPLICATION_JSON
		val successList =  ArrayList<String>()
		try {
			var summaryAll = service.findSummaryAll()
			pdfCoroutine(summaryAll, successList)
			return ResponseEntity
					.ok()
					.headers(headers)
					.body(Gson().toJson(successList))
		}
		catch (ex: Exception) {
			return ResponseEntity
					.badRequest()
					.headers(headers)
					.body(Gson().toJson(ex))

		}
	}

	private fun pdfCoroutine(summaryAll: List<Template7Summary>, successList: ArrayList<String>) = runBlocking {
		val generateChannel = Channel<Template7Summary>()
		launch(CoroutineName("Pipeline")) {
			for (summary in summaryAll) {
				logger.info("Sending Policy No: ${summary.WPNO}")
				generateChannel.send(summary)
			}
			generateChannel.close()
		}

		val t = measureTimeMillis {
			coroutineScope {
				val pdfProperties = properties.pdf
				val workers : Int = pdfProperties!!.workers!!.toInt()
				for (n in 1..workers.toInt()) {
					launch(CoroutineName("pdfWorker-${n}")) { pdfWorker(successList, generateChannel, n) }
				}
			}
		}
		logger.info("Execution generated PDF time: $t ms")
	}

	private suspend fun pdfWorker(successList: ArrayList<String>, generateChannel: ReceiveChannel<Template7Summary>, workerNo: Int) {
		for (summary in generateChannel) {
			logger.info("Generating PDF policy no.: ${summary.WPNO} in thread ${Thread.currentThread().name} : pdfWorker-${workerNo}")

			when (summary) {
				is Template7Summary -> {
					val details = service.findDetailByPolicyNo(summary.WPNO!!)
					val file: String = properties.location + "/" + summary.WPNO + ".pdf"
					val pdf = pdfService.savePdf(file, summary, details, null, DigitialSigned.SIGNED)
					successList.add(pdf)
					logger.info("  PDF : ${pdf}")
				}
			}
		}
	}

	 */
}

