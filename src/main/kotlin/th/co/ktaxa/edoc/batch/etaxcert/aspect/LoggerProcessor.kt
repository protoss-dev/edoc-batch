//package th.co.ktaxa.edoc.batch.etaxcert.aspect
//
//import com.google.gson.Gson
//import io.opentracing.util.GlobalTracer
//import org.aspectj.lang.JoinPoint
//import org.aspectj.lang.ProceedingJoinPoint
//import org.aspectj.lang.annotation.*
//import org.keycloak.KeycloakSecurityContext
//import org.springframework.stereotype.Component
//import org.springframework.util.StopWatch
//import org.springframework.web.context.request.RequestContextHolder
//import org.springframework.web.context.request.ServletRequestAttributes
//import th.co.ktaxa.edoc.batch.etaxcert.auth.getSubjectFromToken
//import th.co.ktaxa.edoc.batch.etaxcert.utility.ILogger
//import th.co.ktaxa.edoc.batch.etaxcert.utility.logger
//import java.io.PrintWriter
//import java.io.StringWriter
//import java.text.SimpleDateFormat
//import java.util.*
//
//@Aspect
//@Component
//class LoggerProcessor : ILogger {
//
////    companion object {
////        val logger = logger()
////    }
//
//    private val sdf = SimpleDateFormat("yyyy/MM/dd hh:mm:ss")
//    private val sdf8601 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
//
//    @Around("@annotation(OpentracingLogger)")
//    @Throws(Throwable::class)
//    fun doLogging(pjp: ProceedingJoinPoint): Any? {
//        val handlerClass = pjp.target.javaClass
//        val handlerName = handlerClass.name
//        val methodName = pjp.signature.name
//        val args = pjp.args
//
//
//        val stopWatch = StopWatch()
//        stopWatch.start()
//
//        val parentSpan = GlobalTracer.get().activeSpan()
//        val newActiveSpan = GlobalTracer.get().buildSpan(methodName)
//                .asChildOf(
//                        parentSpan
//                ).start()
//
//        var result: Any?
//        try{
//
//            result = GlobalTracer.get().scopeManager().activate(newActiveSpan, false).use { scope ->
//                val newSpan = scope.span()
//                newSpan.setTag("handler", handlerName)
//                newSpan.setTag("handler.method_name", methodName)
//                newSpan.setTag("handler.args", args.joinToString(","))
//
//                pjp.proceed()
//            }
//        } catch (e: Throwable) {
//            val sw = StringWriter()
//            e.printStackTrace(PrintWriter(sw))
//            val exceptionAsString = sw.toString()
//
//            newActiveSpan.setTag("is_error", true)
//            newActiveSpan.log(exceptionAsString)
//
//            throw e
//        } finally {
//            stopWatch.stop()
//
//            newActiveSpan.setTag("execution.time", stopWatch.totalTimeMillis)
//            newActiveSpan.finish()
//        }
//
//        return result
//    }
//
//    @Before("execution(public * th.co.ktaxa.edoc.batch.etaxcert.controller.*.*(..))")
//    fun doControllerTagging(jp: JoinPoint) {
//        val handlerClass = jp.target.javaClass
//        val handlerName = handlerClass.name
//        val methodName = jp.signature.name
//        val args = jp.args
//
//        val request = (RequestContextHolder
//                .currentRequestAttributes() as ServletRequestAttributes
//                ).request
//        val keyCloakAttributes: KeycloakSecurityContext? = request.getAttribute(KeycloakSecurityContext::class.java.name) as KeycloakSecurityContext?
//
//        val logger = logger(handlerClass)
//        val jsonBuilder = Gson()
//
//        var userName: String?
//        var userEmail: String?
//        var referenceId: String?
//        when(val token = keyCloakAttributes?.token) {
//            null -> {
//                val auth = getSubjectFromToken(request)
//                userName = auth?.name
//                userEmail = auth?.email
//                referenceId = auth?.sub
//            }
//            else -> {
//                userName = token.name
//                userEmail = token.email
//                referenceId = token.subject
//            }
//        }
//
//        logger.info(jsonBuilder.toJson(mapOf(
//                "timestamp" to sdf8601.format(Date()),
//                "reference_id" to referenceId,
//                "requested_url" to "${request.requestURL}",
//                "http_method" to request.method,
//                "user_name" to userName,
//                "user_email" to userEmail,
//                "handler" to handlerName,
//                "method_name" to methodName,
//                "args" to args.joinToString(","),
//                "message" to "${keyCloakAttributes?.token?.subject} call $methodName"
//        )).toString())
//
//        val currentDate = sdf.format(Date())
//        GlobalTracer.get().activeSpan().let{
//            it.setTag("reference_id", referenceId)
//            it.setTag("user_name", userName)
//            it.setTag("user_email", userEmail)
//            it.setTag("begin", currentDate)
//        }
//    }
//
//    @AfterReturning("execution(public * th.co.ktaxa.edoc.batch.etaxcert.controller.*.*(..))")
//    fun doControllerSuccessLogging(jp: JoinPoint) {
//        val handlerClass = jp.target.javaClass
//        val handlerName = handlerClass.name
//        val methodName = jp.signature.name
//        val args = jp.args
//
//        val request = (RequestContextHolder
//                .currentRequestAttributes() as ServletRequestAttributes
//                ).request
//        val keyCloakAttributes: KeycloakSecurityContext? = request.getAttribute(KeycloakSecurityContext::class.java.name) as KeycloakSecurityContext?
//
//        var userName: String?
//        var userEmail: String?
//        var referenceId: String?
//        when(val token = keyCloakAttributes?.token) {
//            null -> {
//                val auth = getSubjectFromToken(request)
//                userName = auth?.name
//                userEmail = auth?.email
//                referenceId = auth?.sub
//            }
//            else -> {
//                userName = token.name
//                userEmail = token.email
//                referenceId = token.subject
//            }
//        }
//
//        val logger = logger(handlerClass)
//        val jsonBuilder = Gson()
//        logger.info(jsonBuilder.toJson(mapOf(
//                "timestamp" to sdf8601.format(Date()),
//                "reference_id" to referenceId,
//                "requested_url" to "${request.requestURL}",
//                "http_method" to request.method,
//                "user_name" to userName,
//                "user_email" to userEmail,
//                "handler" to handlerName,
//                "method_name" to methodName,
//                "args" to args.joinToString(","),
//                "message" to "${keyCloakAttributes?.token?.subject} call $methodName was success"
//        )).toString())
//
//        val currentDate = sdf.format(Date())
//        GlobalTracer.get().activeSpan().let{
//            it.setTag("success", currentDate)
//            it.setTag("is_error", false)
//        }
//        GlobalTracer.get().activeSpan().finish()
//    }
//
//    @AfterThrowing("execution(public * th.co.ktaxa.edoc.batch.etaxcert.controller.*.*(..))", throwing = "e")
//    fun doControllerErrorLogging(jp: JoinPoint, e: Throwable) {
//        val handlerClass = jp.target.javaClass
//        val handlerName = handlerClass.name
//        val methodName = jp.signature.name
//        val args = jp.args
//
//        val request = (RequestContextHolder
//                .currentRequestAttributes() as ServletRequestAttributes
//                ).request
//        val keyCloakAttributes: KeycloakSecurityContext? = request.getAttribute(KeycloakSecurityContext::class.java.name) as KeycloakSecurityContext?
//        var userName: String?
//        var userEmail: String?
//        var referenceId: String?
//        when(val token = keyCloakAttributes?.token) {
//            null -> {
//                val auth = getSubjectFromToken(request)
//                userName = auth?.name
//                userEmail = auth?.email
//                referenceId = auth?.sub
//            }
//            else -> {
//                userName = token.name
//                userEmail = token.email
//                referenceId = token.subject
//            }
//        }
//
//        val sw = StringWriter()
//        e.printStackTrace(PrintWriter(sw))
//        val exceptionAsString = sw.toString()
//
//        val logger = logger(handlerClass)
//
//        val jsonBuilder = Gson()
//        logger.error(jsonBuilder.toJson(mapOf(
//                "timestamp" to sdf8601.format(Date()),
//                "reference_id" to referenceId,
//                "requested_url" to "${request.requestURL}",
//                "http_method" to request.method,
//                "user_name" to userName,
//                "user_email" to userEmail,
//                "handler" to handlerName,
//                "method_name" to methodName,
//                "args" to args.joinToString(","),
//                "message" to "${keyCloakAttributes?.token?.subject} call $methodName was error",
//                "stackTrace" to exceptionAsString
//        )).toString())
//
//        val currentDate = sdf.format(Date())
//        GlobalTracer.get().activeSpan().let{
//            it.setTag("error_at", currentDate)
//            it.setTag("is_error", true)
//        }
//        GlobalTracer.get().activeSpan().finish()
//    }
//
//}